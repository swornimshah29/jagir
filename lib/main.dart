import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'package:jagir/repository/remote/NetworkService.dart';
import 'package:chopper/chopper.dart';
import 'package:flutter/services.dart';
import 'package:jagir/repository/local/models/mero_job_model/MeroJobModel.dart';
import 'dart:async';

int _selectedIndex = 0;

//changed from new programmer

void main() => runApp(MaterialApp(
      title: 'APP NAME',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    ));

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Widget> pages = [SearchPage(), SearchPage()];

  @override
  Widget build(BuildContext context) {
    ChopperClient service = NetworkService.create().client;

    return MultiProvider(
        providers: [
          Provider<MeroJobApiService>.value(value: service.getService()),
          Provider<FirebaseApiService>.value(value: service.getService()),
        ],
      child: DefaultTabController(
      initialIndex:0,
      length: 5,
      child: Scaffold(
        backgroundColor: Colors.white,
        body: new TabBarView(children: <Widget>[
          BluePage(),
          YellowPage(),
          BluePage(),
          BluePage(),
          // DesignationPage(),
          BluePage()
        ]),
        bottomNavigationBar: Container(
          margin: EdgeInsets.only(bottom: 20),
          child: new TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.home),
              ),
              Tab(
                icon: Icon(Icons.search),
              ),
              Tab(
                icon: Icon(Icons.add),
              ),
              Tab(
                icon: Icon(Icons.favorite),
              ),
              Tab(
                icon: Icon(Icons.perm_identity),
              ),
            ],
            unselectedLabelColor: Colors.black,
            labelColor: Colors.blue,
            indicatorColor: Colors.red,
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            print('clicked');
            MeroJobApiService service=NetworkService.create().client.getService();
            service.getMeroJobPagination().then((response){
              //   MeroJobModelResponse model=MeroJobModelResponse.fromJson(response.body);
              print(response.body);
            }).catchError((error){
              print(error);
            });

          },
        ),
      ),
    )
    );


  }
}

Future<Response> _getJobs(BuildContext context) {
  Future<Response> futureJob =
      Provider.of<MeroJobApiService>(context).getMeroJobPagination();
  return futureJob;
}

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white30,
    );
  }
}

class YellowPage extends StatelessWidget {
  List<Widget> getChips() {
    List<Widget> chips = [];
    for (int i = 0; i < 10; i++) {
      if (i == 0) {
        chips.add(new Container(
            margin: EdgeInsets.only(left: 4, right: 2),
            child: new ChoiceChip(
              selectedColor: Colors.blue,
              selected: true,
              label: Text('Android', style: TextStyle(color: Colors.white)),
              padding: const EdgeInsets.all(8.0),
            )));
      }

      if (i == 1) {
        chips.add(new Container(
            margin: EdgeInsets.only(left: 4, right: 2),
            child: new Chip(
              label: Text('IOS'),
              padding: const EdgeInsets.all(8.0),
            )));
      }

      if (i == 2) {
        chips.add(new Container(
            margin: EdgeInsets.only(left: 4, right: 2),
            child: new Chip(
              label: Text('Web'),
              padding: const EdgeInsets.all(8.0),
            )));
      }

      if (i == 3) {
        chips.add(new Container(
            margin: EdgeInsets.only(left: 4, right: 2),
            child: new Chip(
              label: Text('React'),
              padding: const EdgeInsets.all(8.0),
            )));
      }

      if (i == 4) {
        chips.add(new Container(
            margin: EdgeInsets.only(left: 4, right: 2),
            child: new Chip(
              label: Text('Content writer'),
              padding: const EdgeInsets.all(6.0),
            )));
      }

      if (i == 4) {
        chips.add(new Container(
            margin: EdgeInsets.only(left: 4, right: 2),
            child: new Chip(
              label: Text('PHP developer'),
              padding: const EdgeInsets.all(6.0),
            )));
      }
      if (i == 5) {
        chips.add(new Container(
            margin: EdgeInsets.only(left: 4, right: 2),
            child: new Chip(
              label: Text('PHP developer'),
              padding: const EdgeInsets.all(6.0),
            )));
      }
      if (i == 6) {
        chips.add(new Container(
            margin: EdgeInsets.only(left: 4, right: 2),
            child: new Chip(
              label: Text('PHP developer'),
              padding: const EdgeInsets.all(6.0),
            )));
      }
      if (i == 7) {
        chips.add(new Container(
            margin: EdgeInsets.only(left: 4, right: 2),
            child: new Chip(
              label: Text('Assistant'),
              padding: const EdgeInsets.all(6.0),
            )));
      }

      if (i == 7) {
        chips.add(new Container(
            margin: EdgeInsets.only(left: 4, right: 2),
            child: new Chip(
              label: Text('Banking'),
              padding: const EdgeInsets.all(6.0),
            )));
      }
    }
    return chips;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(color: Colors.white30),
      child: new Wrap(
        direction: Axis.horizontal,
        children: getChips(),
      ),
    );
  }
}


class TechnologyViewPage extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount:3,
      scrollDirection:Axis.horizontal,
      itemBuilder:(context,index){
        return Container(
          width:20,
          height:20,
          color:Colors.red
        );
      }

    );
  }
}

class ColoredTabBar extends Container implements PreferredSizeWidget {
  ColoredTabBar(this.color, this.tabBar);

  final Color color;
  final TabBar tabBar;

  @override
  Size get preferredSize => tabBar.preferredSize;

  @override
  Widget build(BuildContext context) => Container(
        color: color,
        child: tabBar,
      );
}

class BluePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.white, //or set color with: Color(0xFF0000FF)
    ));
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
        brightness: Brightness.light,
        primaryColor:
            Colors.white, //Changing this will change the color of the TabBar
        accentColor: Colors.cyan[600],
      ),
      home: DefaultTabController(
        length: 6,
        child: Scaffold(
            appBar: AppBar(
              elevation: 0,
              bottom: ColoredTabBar(
                Colors.white,
                TabBar(
                  tabs: [
                    Tab(child: Text('Technology')),
                    Tab(child: Text('Engineering')),
                    Tab(child: Text('Teaching')),
                    Tab(child: Text('Banking')),
                    Tab(child: Text('Student')),
                    Tab(child: Text('Hosipitality')),
                  ],
                  indicatorColor: Colors.orange,
                  labelColor: Colors.black,
                  unselectedLabelColor: Colors.black12,
                  isScrollable: true,
                ),
              ),
              title: Text('Find jagir'),
            ),
            body: FutureBuilder<Response>(
                future: Provider.of<MeroJobApiService>(context)
                    .getMeroJobPagination(),
                builder: (context, snapshot) {
                  print(snapshot.data);
                  if (snapshot.connectionState == ConnectionState.done) {
                    print(snapshot.data.body);
                    MeroJobModelResponse model =
                        MeroJobModelResponse.fromJson(snapshot.data.body);
                    print('model message ' + model.message);

                    List<CustomRecyclerView> cards = [];

                    for (var eachModel in model.data) {
                      CustomJobCard2 card = CustomJobCard2(
                          eachModel.designation,
                          eachModel.company,
                          'Details of the company deerwalk',
                          eachModel.views);
                      cards.add(CustomRecyclerView());
                    }

                    return TabBarView(children:cards);
                  } else {
                    // Show a loading indicator while waiting for the posts
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                })),
      ),
    );
  }
}

class CustomViews {
  bool _hasHorizontalScroll;
  List<String> _data;

  CustomViews(this._hasHorizontalScroll, this._data);

  List<String> get data => _data;

  set data(List<String> value) {
    _data = value;
  }

  bool get hasHorizontalScroll => _hasHorizontalScroll;

  set hasHorizontalScroll(bool value) {
    _hasHorizontalScroll = value;
  }

  static List<CustomViews> generateData() {
    CustomViews one = CustomViews(true, ['Data1', 'Data2', 'Data3']);
    CustomViews two = CustomViews(false, ['Data1', 'Data2', 'Data3']);
    CustomViews three = CustomViews(true, ['Data1', 'Data2', 'Data3']);
    CustomViews four = CustomViews(true, ['Data1', 'Data2', 'Data3']);
    CustomViews five = CustomViews(true, ['Data1', 'Data2', 'Data3']);
    CustomViews six = CustomViews(true, ['Data1', 'Data2', 'Data3']);
    return [one, two, three, four, five, six];
  }
}

class CustomRecyclerView extends StatelessWidget {
  List<CustomViews> views = CustomViews.generateData();

  @override
  Widget build(BuildContext context) {
//    return Align(
//      alignment:Alignment.topLeft,
//      child:Container(
//        color:Colors.white30,
//        height:200,
//        child:ListView.builder(
//          scrollDirection:Axis.horizontal,
//          itemCount:5,
//          itemBuilder:(context,index){
//            return Container(
//              margin:EdgeInsets.all(4),
//              color:Colors.white,
//              width:200,
//              child:Card(
//                elevation:2.0,
//              )
//            );
//          }
//        )
//      )
//    );


  //this is bad approach go for nested scrollview
    return Column(
        children: [
          Container(
              color: Colors.white30,
              height: 200,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 5,
                  itemBuilder: (context, index) {
                    return Container(
                        margin: EdgeInsets.all(4),
                        color: Colors.white,
                        width: 200,
                        child: Card(
                          elevation: 2.0,
                        )
                    );
                  }
              )
          ),
          Expanded(
          child:ListView.builder(
          scrollDirection:Axis.vertical,
          itemCount:5,
          itemBuilder:(context,index){
            return Container(
              margin:EdgeInsets.all(4),
              color:Colors.white,
              height:300,
              child:Card(
                elevation:2.0,
              )
            );
          }
        )
          )

        ]
    );
  }

}

class StreamClass {
  StreamController<int> ctrl;

  StreamClass() {
    ctrl = StreamController<int>.broadcast();

    //
    // Initialize a single listener which filters out the odd numbers and
    // only prints the even numbers
    //
    final StreamSubscription subscription = ctrl.stream
        .where((value) => (value % 2 == 0))
        .listen((value) => print('$value'));

    //
    // We here add the data that will flow inside the stream
    //
    for (int i = 1; i < 11; i++) {
      ctrl.sink.add(i);
    }

    //
    // We release the StreamController
    //
    ctrl.close();
  }
}

Stream<int> _someData() async* {
  yield* Stream.periodic(Duration(seconds: 1), (int a) {
    return a++;
  });
}

class MyCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.topCenter,
        child: Card(
          margin: const EdgeInsets.all(8.0),
          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            const ListTile(
              title: Text('Computer programming',
                  style: TextStyle(color: Colors.black)),
              subtitle: Text('Writing software that people dont understand',
                  style: TextStyle(color: Colors.black26)),
            ),
            new Wrap(
              spacing: 2,
              runSpacing: 2,
              children: YellowPage().getChips(),
            )
          ]),
        ));
  }
}

class DesignationPage extends StatelessWidget {
  CustomJobCard2 card;

  DesignationPage(CustomJobCard2 card) {
    this.card = card;
  }

  @override
  Widget build(BuildContext context) {
//    return StreamBuilder(
//      initialData:"Working...",
//      stream:_someData(),
//      builder: (context,snapshot){
//        switch(snapshot.connectionState){
//          case ConnectionState.none:{
//            return Text('none button to start ${snapshot.data.toString()}');
//          }
//
//          case ConnectionState.active:
//          case ConnectionState.waiting:
//          return Text('${snapshot.data.toString()}');
//          case ConnectionState.done:{
//            if(snapshot.hasError)
//              return Text('hasError button to start ${snapshot.data.toString()}');
//          }
//
//        }
//        return Text('Press button to start ${snapshot.data.toString()}');
//      }
//    );

//  return FutureBuilder(
//
//  );

    return ListView(
      children: <Widget>[this.card],
    );
  }
}

class CustomJobCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.topLeft,
        child: Card(
          margin: const EdgeInsets.all(8.0),
          child: Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
            //left part ui
            Container(
                margin: const EdgeInsets.only(left: 4, top: 4, bottom: 4),
                child: ClipRRect(
                  child: Image.asset('images/image3.jpg',
                      alignment: Alignment.topLeft,
                      width: 150,
                      fit: BoxFit.fill //fill till width value
                      ),
                  borderRadius: BorderRadius.all(
                    Radius.circular(6.0),
                  ),
                ),
                height: 195),

            //right part ui
            ListTile(
              title: Text('Computer programming',
                  style: TextStyle(color: Colors.black)),
              subtitle: Text('Writing software that people dont understand',
                  style: TextStyle(color: Colors.black26)),
            )
          ]),
        ));
  }
}

class CustomJobCard2 extends StatelessWidget {
  String _designationName;
  String _companyName;
  String _details;
  String _date;

  CustomJobCard2(
      this._designationName, this._companyName, this._details, this._date);

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: const EdgeInsets.all(8.0),
        child: Container(
            margin: EdgeInsets.all(4),
            height: 150,
            child: Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
              //left ui
              ClipRRect(
                child: Image.asset('images/image3.jpg',
                    // alignment: Alignment.topLeft,
                    width: 150,
                    fit: BoxFit.fill //fill till width value
                    ),
                borderRadius: BorderRadius.all(
                  Radius.circular(4.0),
                ),
              ),

              //fill the remaining width
              Expanded(
                  flex: 2,
                  child: Container(
                      margin: EdgeInsets.only(left: 6),
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          // mainAxisSize: MainAxisSize.max,
                          children: <Widget>[
                            Expanded(
                              flex: 0,
                              child: Text(this._designationName,
                                  style: TextStyle(
                                      color: Colors.black, fontSize: 20)),
                            ),
                            Expanded(
                              flex: 0,
                              child: Text(this._companyName,
                                  style: TextStyle(
                                      color: Colors.black54, fontSize: 18)),
                            ),
                            Expanded(
                                flex: 0,
                                child: Container(
                                  padding: EdgeInsets.only(top: 2),
                                  child: Text(this._details,
                                      style: TextStyle(
                                          color: Colors.black26, fontSize: 16)),
                                )),
                            Expanded(
                                flex: 2,
                                child: Container(
                                  margin: EdgeInsets.only(top: 6),
                                  child: Text('524 Reputations',
                                      style: TextStyle(
                                          color: Colors.red, fontSize: 16)),
                                )),
                            Expanded(
                              flex: 2,
                              child: Container(
                                alignment: Alignment.bottomRight,
                                child: Text(this._date,
                                    style: TextStyle(
                                        color: Colors.orange, fontSize: 16)),
                              ),
                            )
                          ]))),
            ])));
  }
}

class _BodyLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _listTile3(context);
  }
}

class PageViewWidget extends StatefulWidget {
  @override
  State createState() => _PageViewWidgetState();
}

class _PageViewWidgetState extends State<PageViewWidget> {
  void _onPageScrolled(int currentPage) {
    print('onPageScrolled $currentPage');
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: PageController(initialPage: 1),
      onPageChanged: _onPageScrolled,
      children: <Widget>[
        Container(
          child: _BodyLayout(),
        ),
        Container(
          child: _BodyLayout(),
        ),
        Container(
          child: _listTile2(),
        )
      ],
    );
  }
}

class BottomNavigationWidget extends StatefulWidget {
  @override
  State createState() => _BottomNavigationWidgetState();
}

class _BottomNavigationWidgetState extends State<BottomNavigationWidget> {
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

  void _onItemTapped(int index) {
    setState(() {
      print('onitemtapped $index');
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      child: Scaffold(
        body: TabBarView(
          children: <Widget>[
            Container(
              child: _BodyLayout(),
            ),
            Container(
              child: _BodyLayout(),
            ),
            Container(
              child: _listTile2(),
            )
          ],
        ),
        bottomNavigationBar: Container(
          child: new TabBar(tabs: [Tab(icon: Icon(Icons.home))]),
        ),
      ),
    );
  }
}

Widget _listTile1(BuildContext context) {
  return ListView(
    children: <Widget>[
      ListTile(
        title: Text('Sun'),
      ),
      ListTile(
        title: Text('Moon'),
      ),
      ListTile(
        title: Text('Star'),
      ),
    ],
  );
}

Widget _listTile2() {
  return ListView(
    padding: const EdgeInsets.all(8.0),
    children: <Widget>[
      Container(
        height: 50,
        color: Colors.amber[600],
        child: const Center(child: Text('Entry A')),
      ),
      Container(
        height: 50,
        color: Colors.amber[500],
        child: const Center(child: Text('Entry B')),
      ),
      Container(
        height: 50,
        color: Colors.amber[100],
        child: const Center(child: Text('Entry C')),
      ),
    ],
  );
}

Widget _listTile3(BuildContext context) {
  // backing data
  final europeanCountries = [
    'Albania',
    'Andorra',
    'Armenia',
    'Austria',
    'Azerbaijan',
    'Belarus',
    'Belgium',
    'Bosnia and Herzegovina',
    'Bulgaria',
    'Croatia',
    'Cyprus',
    'Czech Republic',
    'Denmark',
    'Estonia',
    'Finland',
    'France',
    'Georgia',
    'Germany',
    'Greece',
    'Hungary',
    'Iceland',
    'Ireland',
    'Italy',
    'Kazakhstan',
    'Kosovo',
    'Latvia',
    'Liechtenstein',
    'Lithuania',
    'Luxembourg',
    'Macedonia',
    'Malta',
    'Moldova',
    'Monaco',
    'Montenegro',
    'Netherlands',
    'Norway',
    'Poland',
    'Portugal',
    'Romania',
    'Russia',
    'San Marino',
    'Serbia',
    'Slovakia',
    'Slovenia',
    'Spain',
    'Sweden',
    'Switzerland',
    'Turkey',
    'Ukraine',
    'United Kingdom',
    'Vatican City'
  ];

  final json = [
    {
      "designation": "Business Development Officer",
      "company": "Lucky Productions",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/marketing-72/"
    },
    {
      "designation": "Business Development Manager",
      "company": "Felt and Yarn",
      "location": "Khusibun, Kathmandu, Central Development Region, Nepal",
      "company_page": "https://merojob.com/business-development-manager-184/"
    },
    {
      "designation": "Business Development Officer",
      "company": "Felt and Yarn",
      "location":
          "Khusibu, Nayabazzar, Kathmandu                                                                                ,                                                                            Khusibun, Kathmandu, Central Development Region, Nepal",
      "company_page": "https://merojob.com/business-development-officer-352/"
    },
    {
      "designation": "Education Counselor",
      "company": "S.D Miracle Educational Consultancy",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/education-counselor-63/"
    },
    {
      "designation": "Corporate Finance Analyst",
      "company": "Raman General",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/corporate-finance-analyst/"
    },
    {
      "designation": "Office Secretary (Female Only)",
      "company": "Universal Visa Consultant Pvt. Ltd",
      "location": "Putalisadak",
      "company_page": "https://merojob.com/office-secretary-222/"
    },
    {
      "designation": "Graphic Designer",
      "company": "Digital Kaligarh",
      "location": "Hadigau, Kathmandu",
      "company_page": "https://merojob.com/graphic-designer-634/"
    },
    {
      "designation": "Tour Executive",
      "company": "Mount Sisne Travels",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/tour-executive-33/"
    },
    {
      "designation": "Customer Care",
      "company": "Mount Sisne Travels",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/customer-care-11/"
    },
    {
      "designation": "Admin Officer",
      "company": "Shakti Food Pvt Ltd",
      "location":
          "25 Patan Industrial Estate, Lagankhel, Lalitpur                                                                                ,                                                                            Lalitpur, Nepal",
      "company_page": "https://merojob.com/admin-officer-111/"
    },
    {
      "designation": "Loan Officer",
      "company": "Hulas Investment Pvt. Ltd",
      "location": "Rajbiraj",
      "company_page": "https://merojob.com/loan-officer-35/"
    },
    {
      "designation": "Corporate Social Responsibility…",
      "company": "A Multinational Company",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/corporate-social-responsibility-manager/"
    },
    {
      "designation": "Lower Secondary Science Teacher",
      "company": "Matribhumi School",
      "location": "Lokanthali Road, Madhyapur Thimi, Bhaktapur, Nepal",
      "company_page":
          "https://merojob.com/teacher-science-lower-secondary-level-female/"
    },
    {
      "designation": "Secondary Teachers",
      "company": "Bodhisattva Vihara (BV)",
      "location":
          "Budhanilkantha (1.5 km North from the Narayanthan Temple), …",
      "company_page":
          "https://merojob.com/secondary-teachers-social-studiesnepali/"
    },
    {
      "designation": "Warehouse Director",
      "company": "China Communications Services (CCS) Nepal",
      "location": "Biratnagar/Nepalgunj/Hetauda/Kathmandu (Choose One)",
      "company_page": "https://merojob.com/warehouse-director-2/"
    },
    {
      "designation": "Business Analyst",
      "company": "COTIVITI NEPAL",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/business-analyst-35/"
    },
    {
      "designation": "Software Engineer (Delphi)",
      "company": "COTIVITI NEPAL",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/software-engineer-delphi/"
    },
    {
      "designation": "Project Manager",
      "company": "COTIVITI NEPAL",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/project-manager-304/"
    },
    {
      "designation": "Sales Officer",
      "company": "SDG Global Imports Pvt. Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/sales-officers-29/"
    },
    {
      "designation": "Warehouse Officer",
      "company": "SDG Global Imports Pvt. Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/warehouse-officer-5/"
    },
    {
      "designation": "Mid-Level Front-end Developer",
      "company": "InfoDevelopers",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/mid-level-front-end-developer-3/"
    },
    {
      "designation": "Computer Hardware Technician",
      "company": "Biz InfoTech",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/computer-hardware-technician-37/"
    },
    {
      "designation": "Mathematics Teacher",
      "company": "Himalayan Whitehouse World School",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/mathematics-teacher-16/"
    },
    {
      "designation": "Business Studies Teacher",
      "company": "Himalayan Whitehouse World School",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/business-studies-teacher-3/"
    },
    {
      "designation": "Community Liaison Specialist",
      "company": "USAID Nepal",
      "location": "Inside and Outside Valley",
      "company_page": "https://merojob.com/community-liaison-specialist-2/"
    },
    {
      "designation": "Medical Sales Representatives",
      "company": "Samar Pharma Company Pvt. Ltd.",
      "location":
          "Kathmandu, Birtamod, Dharan, Biratnagar, Lahan, Janakpur, B…",
      "company_page": "https://merojob.com/medical-sales-representatives/"
    },
    {
      "designation": "Product Manager",
      "company": "Samar Pharma Company Pvt. Ltd.",
      "location": "Lalitpur, Nepal",
      "company_page": "https://merojob.com/product-manager-23/"
    },
    {
      "designation": "Secondary Level Teachers",
      "company": "A Reputed School",
      "location": "Lalitpur, Nepal",
      "company_page": "https://merojob.com/secondary-level-teachers-52/"
    },
    {
      "designation": "Secondary Coordinator",
      "company": "A Reputed School",
      "location": "Lalitpur, Nepal",
      "company_page": "https://merojob.com/secondary-coordinator-2/"
    },
    {
      "designation": "प्रशिक्षार्थी कर्मचारी (सहायक)",
      "company": "बाबियाखर्क बचत तथा ऋण सहकारी संस्था लिमिटेड",
      "location": "शान्तिनगर, काठमाण्डाै",
      "company_page": "https://merojob.com/-2105/"
    },
    {
      "designation": "Engineer (Mechanical)",
      "company": "Sky Light",
      "location": "Thaiba",
      "company_page": "https://merojob.com/engineer-mechanical/"
    },
    {
      "designation": "Photo Editor",
      "company": "S.B. Web Technology",
      "location": "TNT Building, Tinkune, Kathmandu, Nepal",
      "company_page": "https://merojob.com/photo-editor-7/"
    },
    {
      "designation": "Accountant",
      "company": "Shakti Food Pvt Ltd",
      "location":
          "25 Patan Industrial Estate, Lagankhel, Lalitpur                                                                                ,                                                                            Lalitpur, Nepal",
      "company_page": "https://merojob.com/finance-officer-175/"
    },
    {
      "designation": "Administrative Assistant",
      "company": "Javra Software",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/administrative-assistant-94/"
    },
    {
      "designation": "Marketing Executive / Business …",
      "company": "Digi Sense Nepal Pvt. Ltd",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/digital-marketing-executive-20/"
    },
    {
      "designation": "Hotel Manager",
      "company": "Hotel Nana Pokhara",
      "location":
          "Thado Pasal Path, Pokhara                                                                                ,                                                                            Jarebar, Thado Pasal Path, Pokhara",
      "company_page": "https://merojob.com/hotel-manager-17/"
    },
    {
      "designation": "Senior Developer - Android",
      "company": "Attend Pvt Ltd",
      "location": "Lalitpur",
      "company_page": "https://merojob.com/senior-developer-android/"
    },
    {
      "designation": "Senior Developer - iOS",
      "company": "Attend Pvt Ltd",
      "location": "Lalitpur",
      "company_page": "https://merojob.com/senior-ios-developer-19/"
    },
    {
      "designation": "Early Childhood Teacher",
      "company": "Nightingale International Secondary School",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/early-childhood-teacher/"
    },
    {
      "designation": "Nurse",
      "company": "COSMED Laser & Cosmetic Surgery Center",
      "location": "Kalimati, Kathmandu",
      "company_page": "https://merojob.com/nurse-41/"
    },
    {
      "designation": "Human Resources Manager",
      "company": "Attend Pvt Ltd",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/human-resources-manager-14/"
    },
    {
      "designation": "Automation & Data Analyst",
      "company": "ITONICS Nepal",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/automation-data-analyst/"
    },
    {
      "designation": "Secondary Level Science Teachers",
      "company": "Bhaktapur NIST School",
      "location": "Lokanthali, Bhaktapur",
      "company_page":
          "https://merojob.com/elementary-level-and-lower-secondary-science-teachers-2/"
    },
    {
      "designation": "Assistant Graphic Designer",
      "company": "Nanglo International",
      "location": "Sanepa, Lalitpur",
      "company_page": "https://merojob.com/assistant-graphic-designer-4/"
    },
    {
      "designation": "Chartered Accountant",
      "company": "Panchakanya Group",
      "location": "Lalitpur",
      "company_page": "https://merojob.com/chartered-accountant-43/"
    },
    {
      "designation": "Budgetary Control Officer",
      "company": "Panchakanya Group",
      "location": "",
      "company_page": "https://merojob.com/budgetary-control-officer-2/"
    },
    {
      "designation": "Transport/Highway Engineer",
      "company": "FIT Engineering",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/transporthighway-engineer-2/"
    },
    {
      "designation": "Architect Engineer",
      "company": "FIT Engineering",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/architect-engineer-13/"
    },
    {
      "designation": "PHP Developer",
      "company": "GreenCodes",
      "location": "Pepsicola, Kathmandu, Central Development Region, Nepal",
      "company_page": "https://merojob.com/php-developer-235/"
    },
    {
      "designation": "Office Executive Assistant (Fem…",
      "company": "Him Hippo Company",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/office-executive-assistant/"
    },
    {
      "designation": "Operation Officer",
      "company": "Logicabeans",
      "location": "Lal Colony Marg, Durbar Marg, Kathmandu",
      "company_page": "https://merojob.com/operation-officer-29/"
    },
    {
      "designation": "Receptionist / Front Desk Offic…",
      "company": "SDG Global Imports Pvt. Ltd.",
      "location": "Chandol, Bishalnagar, Kathmandu",
      "company_page": "https://merojob.com/receptionist-1154/"
    },
    {
      "designation": "Admin Assistant",
      "company": "Dish Media Network",
      "location": "Bhaisepati",
      "company_page": "https://merojob.com/admin-assistant-94/"
    },
    {
      "designation": "QA Engineer- Data Analytics",
      "company": "Dish Media Network",
      "location": "Bhaisepati",
      "company_page": "https://merojob.com/qa-engineer-data-analytics-2/"
    },
    {
      "designation": "इमर्जेन्सि मेडिकल टेक्निसियन (इ.एम…",
      "company": "नेपाल एम्बुलेन्स सेवा",
      "location": "कालिकास्थान, डिल्लीबजार, काठमाण्डाै",
      "company_page": "https://merojob.com/-2096/"
    },
    {
      "designation": "HR Manager",
      "company": "Hotel Yak & Yeti",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/hr-manager-108/"
    },
    {
      "designation": "Barista",
      "company": "ARYAL INTERNATIONAL HOTEL",
      "location": "New Baneshwor, Kathmandu",
      "company_page": "https://merojob.com/barista-46/"
    },
    {
      "designation": "Senior Marketing Officer - Few",
      "company": "A Reputed Trading Organization",
      "location":
          "Outside Kathmandu Valley                                                                                ,                                                                            Kathmandu, Nepal",
      "company_page": "https://merojob.com/senior-marketing-officer-few/"
    },
    {
      "designation": "Bartender",
      "company": "ARYAL INTERNATIONAL HOTEL",
      "location": "",
      "company_page": "https://merojob.com/bartender-35/"
    },
    {
      "designation": "Micro Enterprise Development (M…",
      "company":
          "Technical Assistance for Micro Enterprise Development Programme",
      "location":
          "Province 1, Province 2, Province 3, Province 4, Province 5,…",
      "company_page":
          "https://merojob.com/micro-enterprise-development-med-officer/"
    },
    {
      "designation": "Social Behaviour Change Communi…",
      "company": "Plan International Nepal",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/social-behaviour-change-communication-sbcc-specialist-for-mca-ii-project-1/"
    },
    {
      "designation": "Business Intelligence (BI) Deve…",
      "company": "Agile Solutions",
      "location": "Gairidhara, Kathmandu",
      "company_page": "https://merojob.com/business-intelligence-bi-developer/"
    },
    {
      "designation": "Project Manager",
      "company": "A Media Company",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/subject-matter-expert-2/"
    },
    {
      "designation": "Admin / Finance Officer",
      "company": "A&A Network",
      "location":
          "Teku, Kathmandu, Central Development Region, Nepal                                                                                ,                                                                            Kathmandu, Nepal",
      "company_page": "https://merojob.com/admin-finance-officer-30/"
    },
    {
      "designation": "Sales Manager",
      "company": "A&A Network",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/sales-manager-225/"
    },
    {
      "designation": "Junior Officer- Sales",
      "company": "Amtrade",
      "location": "Kathmandu & Itahari",
      "company_page": "https://merojob.com/junior-officer-sales-9/"
    },
    {
      "designation": "Senior Accountant",
      "company": "kapsule9",
      "location": "New Road,Kathmandu",
      "company_page": "https://merojob.com/accountant-1455/"
    },
    {
      "designation": "English Content Writer",
      "company": "Top Nepal International",
      "location": "Baluwatar, Kathmandu",
      "company_page": "https://merojob.com/english-content-writer-10/"
    },
    {
      "designation": "Business Development Manager",
      "company": "Lake View Resort",
      "location": "",
      "company_page": "https://merojob.com/business-development-manager-182/"
    },
    {
      "designation": "Marketing Manager",
      "company": "Lake View Resort",
      "location": "Pokhara",
      "company_page": "https://merojob.com/marketing-manager-506/"
    },
    {
      "designation": "Restaurant Manager",
      "company": "Lake View Resort",
      "location": "Pokhara, Nepal",
      "company_page": "https://merojob.com/restaurant-manager-63/"
    },
    {
      "designation": "Sr. Sales Officer / Sales Offic…",
      "company": "Shivam Cements",
      "location": "Pokhara, Nepal",
      "company_page": "https://merojob.com/sr-sales-officer-sales-officer-4/"
    },
    {
      "designation": "Events Coordinator",
      "company": "Janaki Technology",
      "location": "Bakundole, Lalitpur",
      "company_page": "https://merojob.com/event-coordinator-6/"
    },
    {
      "designation": "Cashier (Female)",
      "company": "Laxmee Hair & Beauty Studio Cum Academy",
      "location": "Maharajung Branch",
      "company_page": "https://merojob.com/cashier-female-16/"
    },
    {
      "designation": "Purchase Executive/Procurement …",
      "company": "CBMEW Nepal",
      "location":
          "Sinamangal, Kathmandu, Nepal With frequent travel inside an…",
      "company_page":
          "https://merojob.com/purchase-executiveprocurement-engineer/"
    },
    {
      "designation": "Accountant",
      "company": "CBMEW Nepal",
      "location": "",
      "company_page": "https://merojob.com/accountant-1489/"
    },
    {
      "designation": "Marketing Officer",
      "company": "Holiday Planners Tours and Travels",
      "location": "Airport Gate, Kathmandu",
      "company_page": "https://merojob.com/marketing-and-ticketing-officer/"
    },
    {
      "designation": "Data Scientist",
      "company": "InfoDevelopers",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/data-scientist-4/"
    },
    {
      "designation": "क्यासियर",
      "company": "राप्ती सेभिङ्ग एण्ड क्रेडिट काे-अपरेटिभ लि.",
      "location": "Kumaripati, Lalitpur",
      "company_page": "https://merojob.com/-2109/"
    },
    {
      "designation": "बजार प्रतिनिधि",
      "company": "राप्ती सेभिङ्ग एण्ड क्रेडिट काे-अपरेटिभ लि.",
      "location": "Kumaripati, Lalitpur",
      "company_page": "https://merojob.com/-2110/"
    },
    {
      "designation": "रिसेप्सनिष्ट",
      "company": "राप्ती सेभिङ्ग एण्ड क्रेडिट काे-अपरेटिभ लि.",
      "location": "Kumaripati, Lalitpur",
      "company_page": "https://merojob.com/-2111/"
    },
    {
      "designation": "Nepal Program Manager-Based in …",
      "company": "Saferworld",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/nepal-program-manager-based-in-kathmandu/"
    },
    {
      "designation": "Finance and Administration Offi…",
      "company": "Saferworld",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/finance-and-administration-officer-based-in-kathmandu/"
    },
    {
      "designation": "Graphic Designer",
      "company": "Amtech Med (P.) Ltd.",
      "location": "Biratnagar, Nepal",
      "company_page": "https://merojob.com/graphic-designer-632/"
    },
    {
      "designation": "Junior Sales Manager",
      "company": "Ashok Steel Industries",
      "location":
          "Kathmandu, Nepalgunj, Dhangadhi, Janakpur, Pokhara, Surkhet…",
      "company_page": "https://merojob.com/junior-sales-manager-few-2/"
    },
    {
      "designation": "Technical Advisor - ASRHR",
      "company": "GIZ",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/technical-advisor-asrhr/"
    },
    {
      "designation": "Technical Advisor - ASRHR",
      "company": "GIZ",
      "location": "Dhangadi",
      "company_page": "https://merojob.com/technical-advisor-asrhr-2/"
    },
    {
      "designation": "Receptionist (Female)",
      "company": "Raffles Educare Associates",
      "location": "Putalisadak, Kathmandu, Nepal",
      "company_page": "https://merojob.com/receptionist-female-158/"
    },
    {
      "designation": "HR Executive",
      "company": "merojob",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/hr-executive-39/"
    },
    {
      "designation": "Social Media Manager (Female)",
      "company": "COSMED Laser & Cosmetic Surgery Center",
      "location": "Kalimati, Kathmandu, Nepal",
      "company_page": "https://merojob.com/social-media-manager-21/"
    },
    {
      "designation": "Manager-AML and Compliance Depa…",
      "company": "Shikhar Insurance Co. Ltd.",
      "location": "Shikhar Biz Center, Kathmandu",
      "company_page":
          "https://merojob.com/manager-aml-and-compliance-department/"
    },
    {
      "designation": "Assistant Teachers",
      "company": "+2 College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/assistant-teachers-9/"
    },
    {
      "designation": "Staffs (Academic Science Counse…",
      "company": "+2 College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/staff-3/"
    },
    {
      "designation": "Teachers",
      "company": "+2 College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/teachers-74/"
    },
    {
      "designation": "Assistant Manager-Finance Depar…",
      "company": "Shikhar Insurance Co. Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/assistant-manager-finance-department/"
    },
    {
      "designation": "Lab Incharge",
      "company": "+2 College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/lab-incharge-4/"
    },
    {
      "designation": "Consultant Ophthalmologist",
      "company": "Chitwan Eye Hospital",
      "location": "Chitwan, Nepal",
      "company_page": "https://merojob.com/consultant-ophthalmologist-3/"
    },
    {
      "designation": "Accounting Officer",
      "company": "Swadhin Saving and Credit Co-Operative Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/accounting-officer-6/"
    },
    {
      "designation": "Associate Legal Officer",
      "company": "Dish Media Network Ltd.",
      "location": "Bhaisepati, Lalitpur",
      "company_page": "https://merojob.com/associate-legal-officer/"
    },
    {
      "designation": "Admin Assistant",
      "company": "Dish Media Network Ltd.",
      "location": "Bhaisepati, Lalitpur",
      "company_page": "https://merojob.com/admin-assistant-93/"
    },
    {
      "designation": "Associate Procurement Officer",
      "company": "Dish Media Network Ltd.",
      "location": "Bhaisepati, Lalitpur",
      "company_page": "https://merojob.com/associate-procurement-officer/"
    },
    {
      "designation": "Team Member - Legal Department …",
      "company": "Kamana Sewa Bikas Bank Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/team-member-legal-department-assistant-to-supervisor-few/"
    },
    {
      "designation": "Receptionist / Front Desk Offic…",
      "company": "A.G.R Industries",
      "location": "Sanepa, Lalitpur",
      "company_page": "https://merojob.com/receptionist-front-desk-9/"
    },
    {
      "designation": "Store Incharge",
      "company": "A.G.R Industries",
      "location": "Sanepa, Lalitpur",
      "company_page": "https://merojob.com/store-incharge-51/"
    },
    {
      "designation": "Sales Supervisor",
      "company": "Phoenix Distillery",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/sales-officer-423/"
    },
    {
      "designation": "Sales Executive (Female)",
      "company": "Arya Pharma",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/sales-executive-female-31/"
    },
    {
      "designation": "Volunteers",
      "company": "Raleigh International Nepal",
      "location": "Gorkha & Makwanpur Districts",
      "company_page": "https://merojob.com/volunteers-23/"
    },
    {
      "designation": "Marketing Executive",
      "company": "Reality Escape",
      "location": "Keshar Mahal Marga, Thamel, Kathmandu",
      "company_page": "https://merojob.com/marketing-executive-815/"
    },
    {
      "designation": "BDS Doctor",
      "company": "Om Samaj Dental Hospital",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/bds-doctor-2/"
    },
    {
      "designation": "Endodontist",
      "company": "Om Samaj Dental Hospital",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/endodontist/"
    },
    {
      "designation": "Accountant",
      "company": "Om Samaj Dental Hospital",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/accountant-1485/"
    },
    {
      "designation": "Recovery Audit Trainee",
      "company": "Flextecs Nepal",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/recovery-audit-trainee-23/"
    },
    {
      "designation": "Junior Field Officer",
      "company": "MITRA Samaj",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/junior-field-officer/"
    },
    {
      "designation": "Engineers",
      "company": "Towa Engineering Nepal",
      "location": "Tripureshwor, Kathmandu",
      "company_page": "https://merojob.com/engineers-12/"
    },
    {
      "designation": "Full Stack Web Developer",
      "company": "ZEN IT HUB",
      "location": "Baluwatar, Kathmandu",
      "company_page": "https://merojob.com/full-stack-web-developer-7/"
    },
    {
      "designation": "Restaurant Manager",
      "company": "Fast Food Restaurant",
      "location": "Pokhara, Nepal",
      "company_page": "https://merojob.com/restaurant-manager-62/"
    },
    {
      "designation": "Full stack Developer",
      "company": "MyriadX",
      "location": "Trade Tower, Thapathali, Kathmandu",
      "company_page": "https://merojob.com/full-stack-developer-19/"
    },
    {
      "designation": "Data Analyst",
      "company": "MyriadX",
      "location": "Trade Tower Thapathali, Kathmandu",
      "company_page": "https://merojob.com/data-analyst-18/"
    },
    {
      "designation": "Quality Assurance",
      "company": "MyriadX",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/quality-assurance-14/"
    },
    {
      "designation": "Project Manager",
      "company": "MyriadX",
      "location": "",
      "company_page": "https://merojob.com/project-manager-302/"
    },
    {
      "designation": "Chemistry Teacher",
      "company": "Himalayan Whitehouse World School",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/chemistry-teacher-14/"
    },
    {
      "designation": "Biology Teacher",
      "company": "Himalayan Whitehouse World School",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/biology-teacher-5/"
    },
    {
      "designation": "Physics Teacher",
      "company": "Himalayan Whitehouse World School",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/physics-teacher-8/"
    },
    {
      "designation": "Economics Teacher",
      "company": "Himalayan Whitehouse World School",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/economics-teacher-11/"
    },
    {
      "designation": "Computer Teacher",
      "company": "Himalayan Whitehouse World School",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/computer-teacher-59/"
    },
    {
      "designation": "Accountancy Teacher",
      "company": "Himalayan Whitehouse World School",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/accountancy-teacher-7/"
    },
    {
      "designation": "English Teacher (Lower Secondar…",
      "company": "English Medium School",
      "location": "Dhumbarahi, Kathmandu",
      "company_page":
          "https://merojob.com/english-teacher-lower-secondary-and-secondary/"
    },
    {
      "designation": "English Teacher (Secondary)",
      "company": "English Medium School",
      "location": "Dhumbarahi, Kathmandu",
      "company_page": "https://merojob.com/english-teacher-secondary-7/"
    },
    {
      "designation": "Office Secretary cum Public Rel…",
      "company": "Intermanagement Manpower Services Pvt. Ltd.",
      "location": "Gongabu, Kathmandu",
      "company_page": "https://merojob.com/public-relations-officer-7/"
    },
    {
      "designation": "Customer Service Representative…",
      "company": "Saturn Outsource",
      "location": "Kamalpokhari, Kathmandu",
      "company_page":
          "https://merojob.com/customer-service-representative-csr-40/"
    },
    {
      "designation": "Quality Analyst",
      "company": "Exolutus",
      "location": "Exolutus Pvt. Ltd. Lalupate marga,Hattisar",
      "company_page": "https://merojob.com/quality-analyst-34/"
    },
    {
      "designation": "Science Teacher",
      "company": "Kathmandu Valley School",
      "location": "Maharajgunj, Chakrapath",
      "company_page": "https://merojob.com/teacher-90/"
    },
    {
      "designation": "Business Development Officer",
      "company": "Technovate International Pvt. Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/business-development-officer-348/"
    },
    {
      "designation": "Traineer Sales & Assistant Proj…",
      "company": "Datalytics",
      "location": "2nd Floor, Near ACE college , Baneshwor",
      "company_page":
          "https://merojob.com/traineer-sales-assistant-project-manager/"
    },
    {
      "designation": "Civil Engineer",
      "company": "House of Interiors",
      "location":
          "Kathmandu                                                                                ,                                                                            Chitwan",
      "company_page": "https://merojob.com/civil-engineer-374/"
    },
    {
      "designation": "Corporate Sales and Marketing O…",
      "company": "Gokarna House Restaurant",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/corporate-sales-and-marketing-officer-female/"
    },
    {
      "designation": "Project Manager",
      "company": "House of Interiors",
      "location": "Chitwan, Kathmandu",
      "company_page": "https://merojob.com/project-manager-303/"
    },
    {
      "designation": "IT Infra Executive",
      "company": "A Multinational Company",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/it-infra-executive/"
    },
    {
      "designation": "Human Resource / Operations Man…",
      "company": "An Advertisement Studio",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/human-resource-operations-manager-2/"
    },
    {
      "designation": "Program Coordinator",
      "company": "Clean up Nepal",
      "location": "Golfutar",
      "company_page": "https://merojob.com/program-coordinator-80/"
    },
    {
      "designation": "Marketing Executive",
      "company": "Social Aves",
      "location": "Jawalakhel, Lalitpur, Nepal",
      "company_page": "https://merojob.com/marketing-executive-816/"
    },
    {
      "designation": "Sales/ Marketing Officer (Femal…",
      "company": "Sayon Enterprises",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/sales-marketing-officer-female-2/"
    },
    {
      "designation": "Senior Accountant",
      "company": "Genex Investment",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/senior-accountant-202/"
    },
    {
      "designation": "Senior Officer - Business Devel…",
      "company": "Janaki Technology",
      "location": "Bakundole, Lalitpur",
      "company_page": "https://merojob.com/senior-officer-business-development/"
    },
    {
      "designation": "Team Leader, The Global Fund Tu…",
      "company": "National Association of PLWHA in Nepal",
      "location":
          "Dhangadi with frequent travel to NAP+N partner CBOs under G…",
      "company_page":
          "https://merojob.com/team-leader-the-global-fund-tuberculosis-program/"
    },
    {
      "designation": "Finance and Admin Manager, The …",
      "company": "National Association of PLWHA in Nepal",
      "location":
          "Kathmandu with frequent travel to NAP+N partner CBOs under …",
      "company_page":
          "https://merojob.com/finance-and-admin-manager-the-global-fund-hiv-program/"
    },
    {
      "designation": "Civil Engineer",
      "company": "PACE Consultant",
      "location": "Kathmandu",
      "company_page": "https://merojob.com/civil-engineer-377/"
    },
    {
      "designation": "Computer Operator",
      "company": "PACE Consultant",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/computer-operator-117/"
    },
    {
      "designation": "Architect (Few)",
      "company": "PACE Consultant",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/architect-few-6/"
    },
    {
      "designation": "Finance Manager",
      "company": "Nepal Hydro and Electric Limited",
      "location": "Butwal, Nepal",
      "company_page": "https://merojob.com/finance-manager-145/"
    },
    {
      "designation": "Business Development Officer",
      "company": "Good Neighbors Nepal",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/business-development-officer-351/"
    },
    {
      "designation": "Documentation Officer",
      "company": "Machhapuchchhre School",
      "location": "Kusunti, Lalitpur",
      "company_page": "https://merojob.com/documentation-officer-69/"
    },
    {
      "designation": "Secondary Level Mathematics Tea…",
      "company": "Machhapuchchhre School",
      "location": "Kusunti, Lalitpur",
      "company_page": "https://merojob.com/grade-teacher-66/"
    },
    {
      "designation": "District Officer",
      "company": "Sunaulo Parivar Nepal (SPN)",
      "location": "Dailekh",
      "company_page": "https://merojob.com/district-officer-6/"
    },
    {
      "designation": "Lighting/Rendering Artist",
      "company": "Makarios",
      "location": "Naxal, Kathmandu",
      "company_page": "https://merojob.com/lightingrendering-artist/"
    },
    {
      "designation": "Animator",
      "company": "Makarios",
      "location": "Naxal, Kathmandu",
      "company_page": "https://merojob.com/animator-5/"
    },
    {
      "designation": "Software Engineer",
      "company": "F1Soft International",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/software-engineer-80/"
    },
    {
      "designation": "Associate Software Engineer",
      "company": "F1Soft International",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/associate-software-engineer/"
    },
    {
      "designation": "Senior Software Engineer",
      "company": "F1Soft International",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/senior-software-engineer-36/"
    },
    {
      "designation": "Web Content Management Assistan…",
      "company":
          "The International Centre for Integrated Mountain Development (ICIMOD)",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/web-content-management-assistant/"
    },
    {
      "designation": "Legal Aid Officer (Level 6th)",
      "company":
          "Nepal Federation of Savings and Credit Cooperative Unions Limited (NEFSCUN)",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/legal-aid-officer-level-6th/"
    },
    {
      "designation": "Business Officer (Level 6th)",
      "company":
          "Nepal Federation of Savings and Credit Cooperative Unions Limited (NEFSCUN)",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/business-officer-level-6th/"
    },
    {
      "designation": "Health And Safety Officer /Supe…",
      "company": "SafBon Water Service (Holding) Inc.",
      "location": "Sankhamul, Kathmandu",
      "company_page":
          "https://merojob.com/health-and-safety-officer-supervisor/"
    },
    {
      "designation": "QA/QC Engineer/Supervisor",
      "company": "SafBon Water Service (Holding) Inc.",
      "location": "Sankhamul, Kathmandu",
      "company_page": "https://merojob.com/qaqc-engineersupervisor/"
    },
    {
      "designation": "Chinese Language Translator",
      "company": "SafBon Water Service (Holding) Inc.",
      "location": "Sankhamul, Kathmandu",
      "company_page": "https://merojob.com/chinese-language-translator-3/"
    },
    {
      "designation": "Hardware and Network Technician",
      "company": "Vianet Communications",
      "location": "Jawalakhel, Lalitpur",
      "company_page": "https://merojob.com/hardware-and-network-technician-21/"
    },
    {
      "designation": "Program Officer",
      "company": "MyRight",
      "location": "Kathmandu with frequent field visits",
      "company_page": "https://merojob.com/program-officer-90/"
    },
    {
      "designation": "Business Development Manager",
      "company": "OYO Rooms",
      "location":
          "Kathmandu                                                                                ,                                                                            Birgunj                                                                                ,                                                                            Biratnagar                                                                                ,                                                                            Janakpur",
      "company_page": "https://merojob.com/business-development-manager-183/"
    },
    {
      "designation": "Makeup Artist",
      "company": "AMSA Academy",
      "location": "Gyaneshwor",
      "company_page": "https://merojob.com/makeup-artist/"
    },
    {
      "designation": "IELTS Instructor",
      "company": "Impulse ktm",
      "location": "Putalisadak, Kathmandu",
      "company_page": "https://merojob.com/ielts-instructor-77/"
    },
    {
      "designation": "Accountant",
      "company": "Traditional Home",
      "location": "Patan, Lalitpur",
      "company_page": "https://merojob.com/accountant-1488/"
    },
    {
      "designation": "Paid Internship",
      "company": "LocalLoyalties",
      "location": "Chitwan",
      "company_page": "https://merojob.com/paid-internship-9/"
    },
    {
      "designation": "Senior Full Stack Developer",
      "company": "LocalLoyalties",
      "location": "Chitwan, Nepal",
      "company_page": "https://merojob.com/senior-full-stack-developer-2/"
    },
    {
      "designation": "Android Developer",
      "company": "LocalLoyalties",
      "location": "Chitwan, Nepal",
      "company_page": "https://merojob.com/android-developer-128/"
    },
    {
      "designation": "Newspaper Subscription Manager",
      "company": "Nepal Education Times",
      "location": "Kathmandu Nepal",
      "company_page": "https://merojob.com/newspaper-subscription-manager-2/"
    },
    {
      "designation": "HR/Guarding Manager",
      "company": "Shikhar Himalayan Security",
      "location": "Bhaktapur, Nepal",
      "company_page": "https://merojob.com/hrguarding-manager/"
    },
    {
      "designation": "Content Writer",
      "company": "Discovery World Trekking",
      "location": "Work From Home",
      "company_page": "https://merojob.com/content-writer-222/"
    },
    {
      "designation": "General Manager",
      "company": "Neo Saving and Credit Cooperative Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/general-manager-168/"
    },
    {
      "designation": "Trainee Assistant",
      "company": "Mega Bank Nepal Limited",
      "location": "",
      "company_page": "https://merojob.com/trainee-assistant-45/"
    },
    {
      "designation": "Management Trainee",
      "company": "Mega Bank Nepal Limited",
      "location": "",
      "company_page": "https://merojob.com/management-trainee-85/"
    },
    {
      "designation": "Hostel Warden",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/hostel-warden-37/"
    },
    {
      "designation": "Librarian",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/librarian-102/"
    },
    {
      "designation": "Counselors",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/counselors-15/"
    },
    {
      "designation": "Coordinators",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/coordinators-5/"
    },
    {
      "designation": "IT Officer",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/it-officer-341/"
    },
    {
      "designation": "MIS Officer",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/mis-officer-14/"
    },
    {
      "designation": "Deputy Chief Risk Officer (Depu…",
      "company": "NIC ASIA BANK Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/deputy-chief-risk-officer-deputy-manager-manager/"
    },
    {
      "designation": "Deputy Head of Internal Audit (…",
      "company": "NIC ASIA BANK Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/deputy-head-of-internal-audit-assistant-managerdeputy-manager/"
    },
    {
      "designation": "Communication Officer",
      "company": "WWF",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/communication-officer-56/"
    },
    {
      "designation": "Head Provincial Assurance (Seni…",
      "company": "NIC ASIA BANK Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/head-provincial-assurance-senior-officer-assistant-manager/"
    },
    {
      "designation": "Chief of Sub Provincial Office …",
      "company": "NIC ASIA BANK Ltd.",
      "location": "Sub provincial Office under Province Office",
      "company_page":
          "https://merojob.com/chief-of-sub-provincial-office-senior-officer-assistant-manager/"
    },
    {
      "designation": "Deputy Head of DNA and Talent M…",
      "company": "NIC ASIA BANK Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/deputy-head-of-dna-and-talent-management-human-resources-officer/"
    },
    {
      "designation": "Electrical Engineer (Site)",
      "company": "Peoples Hydropower Company Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/electrical-engineer-site/"
    },
    {
      "designation": "Mechanical Engineer (Site)",
      "company": "Peoples Hydropower Company Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/mechanical-engineer-site/"
    },
    {
      "designation": "Civil Engineer (Site)",
      "company": "Peoples Hydropower Company Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/civil-engineer-site-3/"
    },
    {
      "designation": "Design Engineer (Kathmandu/Site)",
      "company": "Peoples Hydropower Company Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/design-engineer-kathmandusite/"
    },
    {
      "designation": "Logistics/Stores Manager/Officer",
      "company": "Hukam Pharmaceuticals Private Limited",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/logisticsstores-managerofficer/"
    },
    {
      "designation": "Mechanical/Electrical Engineer",
      "company": "Hukam Pharmaceuticals Private Limited",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/mechanicalelectrical-engineer/"
    },
    {
      "designation": "Store Officer",
      "company": "Jawalakhel Group of Industries",
      "location": "Chitwan, Nawalparasi",
      "company_page": "https://merojob.com/store-officer-19/"
    },
    {
      "designation": "Electrical / Mechanical Supervi…",
      "company": "Peoples Hydropower Company Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/electrical-mechanical-supervisor-kathmandusite/"
    },
    {
      "designation": "Sr. Geologist (Site)",
      "company": "Peoples Hydropower Company Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/sr-geologist-site/"
    },
    {
      "designation": "Factory Manager",
      "company": "Jawalakhel Group of Industries",
      "location":
          "Nawalparasi, Nepal                                                                                ,                                                                            Kathmandu, Nepal",
      "company_page": "https://merojob.com/faculty-manager/"
    },
    {
      "designation": "Geologist (Site)",
      "company": "Peoples Hydropower Company Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/geologist-site/"
    },
    {
      "designation": "Quality Control Engineer",
      "company": "Peoples Hydropower Company Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/quality-control-engineer-19/"
    },
    {
      "designation": "Primary School In charge",
      "company": "Kathmandu Meridian School",
      "location": "Bansbari, Chakrapath",
      "company_page": "https://merojob.com/primary-school-in-charge-2/"
    },
    {
      "designation": "Social Teacher - Lower Secondar…",
      "company": "Kathmandu Meridian School",
      "location": "Bansbari, Chakrapath",
      "company_page":
          "https://merojob.com/social-teacher-lower-secondary-level/"
    },
    {
      "designation": "Driver (One)- based in Sarlahi …",
      "company": "World Vision International Nepal",
      "location": "Sarlahi, Nepal",
      "company_page":
          "https://merojob.com/driver-one-based-in-sarlahi-district/"
    },
    {
      "designation": "Regional Sales Manager",
      "company": "Triage Meditech",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/regional-sales-manager-47/"
    },
    {
      "designation": "Wound Management Trainer",
      "company": "Triage Meditech",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/wound-management-trainer/"
    },
    {
      "designation": "Human Resource Manager",
      "company": "Nepal Cancer Hospital",
      "location": "Harisiddhi, Lalitpur, Nepal",
      "company_page": "https://merojob.com/human-resource-manager-85/"
    },
    {
      "designation": "Office Assistant",
      "company": "Atlas Saving and Credit Cooperative",
      "location": "Kathmandu",
      "company_page": "https://merojob.com/office-assistant-352/"
    },
    {
      "designation": "Account Officer",
      "company": "Alfa Beta Institute",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/account-officer-319/"
    },
    {
      "designation": "Secondary Level Mathematics Tea…",
      "company": "Bhaktapur NIST School",
      "location": "Lokanthali, Bhaktapur",
      "company_page": "https://merojob.com/secondary-teachers-for-mathematics/"
    },
    {
      "designation": "Secondary Social and English Te…",
      "company": "Bhaktapur NIST School",
      "location": "Lokanthali, Bhaktapur",
      "company_page":
          "https://merojob.com/secondary-teachers-for-social-and-english/"
    },
    {
      "designation": "Lower Secondary Math and Scienc…",
      "company": "Bhaktapur NIST School",
      "location": "LOKANTHALI, BHAKTAPUR",
      "company_page":
          "https://merojob.com/lower-secondary-teacher-for-math-and-science/"
    },
    {
      "designation": "Primary Level Science and Math …",
      "company": "Bhaktapur NIST School",
      "location": "LOKANTHALI, BHAKTAPUR",
      "company_page":
          "https://merojob.com/primary-level-science-and-math-teacher-2/"
    },
    {
      "designation": "Civil Engineer",
      "company": "FIT Engineering",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/civil-engineer-375/"
    },
    {
      "designation": "Surveyor",
      "company": "FIT Engineering",
      "location": "",
      "company_page": "https://merojob.com/surveyor-50/"
    },
    {
      "designation": "Service Engineer",
      "company": "Medical Suppliers & Services",
      "location": "Manbhawan, Lalitpur",
      "company_page": "https://merojob.com/service-engineer-44/"
    },
    {
      "designation": "Functional ERP Analyst",
      "company": "Triveni Group",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/functional-erp-analyst-2/"
    },
    {
      "designation": "Senior Accounts Officer",
      "company": "InfraSoft Solutions Pvt. Ltd.",
      "location": "Jawgal, Lalitpur",
      "company_page": "https://merojob.com/senior-accounts-officer-10/"
    },
    {
      "designation": "Business Development Officer",
      "company": "InfraSoft Solutions Pvt. Ltd.",
      "location": "Jawgal, Lalitpur",
      "company_page": "https://merojob.com/business-development-officer-350/"
    },
    {
      "designation": "Teachers",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/teachers-75/"
    },
    {
      "designation": "CCA Officer",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/cca-officer/"
    },
    {
      "designation": "Content Writer",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/content-writer-224/"
    },
    {
      "designation": "Research & Analysis-Academics",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/research-analysis-academics/"
    },
    {
      "designation": "Research & Analysis-Administrat…",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/research-analysis-administrative/"
    },
    {
      "designation": "Store Keepers",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/store-keepars/"
    },
    {
      "designation": "Civil Engineer",
      "company": "SK Ventures",
      "location": "Tangal, Gairidhara, Kathmandu",
      "company_page": "https://merojob.com/civil-engineer-372/"
    },
    {
      "designation": "Finance Consultant",
      "company": "SK Ventures",
      "location": "Tangal, Gairidhara, Kathmandu",
      "company_page": "https://merojob.com/finance-consultant/"
    },
    {
      "designation": "Sales Officers",
      "company": "SK Ventures",
      "location": "Tangal, Gairidhara, Kathmandu",
      "company_page": "https://merojob.com/sales-officers-28/"
    },
    {
      "designation": "Copywriter/ Content Writer",
      "company": "ANS Creation",
      "location": "Thapagaun, Baneswor",
      "company_page": "https://merojob.com/copywriter-16/"
    },
    {
      "designation": "Sales & Marketing Head",
      "company": "Valley Cold Store ( Valley Group )",
      "location": "Balaju",
      "company_page": "https://merojob.com/sales-marketing-head-7/"
    },
    {
      "designation": "Economist/Political Economy Ana…",
      "company": "CAMRIS International",
      "location": "Kathmandu",
      "company_page": "https://merojob.com/economistpolitical-economy-analyst/"
    },
    {
      "designation": "HR Manager",
      "company": "Amtech Med (P.) Ltd.",
      "location": "Biratnagar, Nepal",
      "company_page": "https://merojob.com/hr-manager-109/"
    },
    {
      "designation": "IPQA Pharmacist",
      "company": "Amtech Med (P.) Ltd.",
      "location": "Biratnagar, Nepal",
      "company_page": "https://merojob.com/ipqa-pharmacist/"
    },
    {
      "designation": "Mechanical/ Electrical Sub-Engi…",
      "company": "Amtech Med (P.) Ltd.",
      "location": "Biratnagar, Nepal",
      "company_page": "https://merojob.com/mechanical-electrical-sub-engineer/"
    },
    {
      "designation": "Production Pharmacist (Veterina…",
      "company": "Amtech Med (P.) Ltd.",
      "location": "Biratnagar, Nepal",
      "company_page": "https://merojob.com/production-pharmacist-veterinary/"
    },
    {
      "designation": "F, R & D Pharmacist",
      "company": "Amtech Med (P.) Ltd.",
      "location": "Biratnagar, Nepal",
      "company_page": "https://merojob.com/f-r-d-pharmacist/"
    },
    {
      "designation": "Senior Pharmacist (QA)",
      "company": "Amtech Med (P.) Ltd.",
      "location": "Biratnagar, Nepal",
      "company_page": "https://merojob.com/senior-pharmacist-qa/"
    },
    {
      "designation": "Grade Teachers",
      "company": "The New Summit Secondary School",
      "location": "Maitidevi Chowk, Kathmandu",
      "company_page": "https://merojob.com/grade-teachers-53/"
    },
    {
      "designation": "Grade Teachers",
      "company": "The New Summit Secondary School",
      "location": "Maitidevi Chowk, Kathmandu",
      "company_page": "https://merojob.com/grade-teachers-54/"
    },
    {
      "designation": "School Supervisor",
      "company": "The New Summit Secondary School",
      "location": "Maitidevi Chowk, Kathmandu",
      "company_page": "https://merojob.com/school-supervisor-2/"
    },
    {
      "designation": "Exam In-Charge",
      "company": "The New Summit Secondary School",
      "location": "Maitidevi Chowk, Kathmandu",
      "company_page": "https://merojob.com/exam-in-charge/"
    },
    {
      "designation": "Overseer",
      "company": "Kedia Organisation",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/overseer-32/"
    },
    {
      "designation": "Media Manager",
      "company": "Equal Access International (EIA)",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/media-manager-3/"
    },
    {
      "designation": "Office Secretary",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/office-secretary-223/"
    },
    {
      "designation": "Nurse",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/nurse-42/"
    },
    {
      "designation": "Event Manager",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/event-manager-23/"
    },
    {
      "designation": "Psychologist",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/psychologist-7/"
    },
    {
      "designation": "ECA In-Charge",
      "company": "Pentagon International College",
      "location": "",
      "company_page": "https://merojob.com/eca-in-charge-6/"
    },
    {
      "designation": "Students Support Officer",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/students-support-officer/"
    },
    {
      "designation": "Architect",
      "company": "Archiplan-SBT Pvt Ltd",
      "location": "Niketan Marg-210, Charkhal, Dillibazaar",
      "company_page": "https://merojob.com/architect-270/"
    },
    {
      "designation": "Sales and Marketing Team Leader",
      "company": "API Business",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/sales-and-marketing-team-leader/"
    },
    {
      "designation": "Dot Net (.NET) Software Enginee…",
      "company": "Dolphin Dive Technology",
      "location": "Jhamsikhel, Lalitpur",
      "company_page":
          "https://merojob.com/dot-net-net-software-engineer-fresher/"
    },
    {
      "designation": "Site Accountant",
      "company": "Raman Construction",
      "location": "ILAM- Bazar",
      "company_page": "https://merojob.com/site-accountant-37/"
    },
    {
      "designation": "Php / laravel Developers",
      "company": "Computalaya Network",
      "location": "",
      "company_page": "https://merojob.com/php-laravel-developers/"
    },
    {
      "designation": "Customer Counselor (Written Eng…",
      "company": "Yodha",
      "location": "New Baneshwor",
      "company_page":
          "https://merojob.com/customer-counselor-written-english-22/"
    },
    {
      "designation": "Sales Officer",
      "company": "Kedia Organisation",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/sales-officer-425/"
    },
    {
      "designation": "Civil Engineer",
      "company": "Kedia Organisation",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/civil-engineer-376/"
    },
    {
      "designation": "Deputy Chief Executive Officer",
      "company": "NCC Bank",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/deputy-chief-executive-officer-12/"
    },
    {
      "designation": "Deputy General Manager",
      "company": "NCC Bank",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/deputy-general-manager-16/"
    },
    {
      "designation": "Assistant Manager (For Far West…",
      "company": "NCC Bank",
      "location": "Far Western region",
      "company_page":
          "https://merojob.com/assistant-manager-for-far-western-region/"
    },
    {
      "designation": "Management Trainee",
      "company": "NCC Bank",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/management-trainee-87/"
    },
    {
      "designation": "National Expert Consultant",
      "company": "CAMRIS International",
      "location": "Kathmandu",
      "company_page": "https://merojob.com/national-expert-consultant/"
    },
    {
      "designation": "Researcher Consultant",
      "company": "CAMRIS International",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/researcher-consultant/"
    },
    {
      "designation": "Counselor",
      "company": "Aids Healthcare Foundation (AHF)",
      "location": "Butwal",
      "company_page": "https://merojob.com/counselor-294/"
    },
    {
      "designation": "PHP Developer",
      "company": "EduWise Nepal",
      "location": "",
      "company_page": "https://merojob.com/php-developer-234/"
    },
    {
      "designation": "Associate Broadcast Engineer",
      "company": "Dish Media Network",
      "location": "",
      "company_page": "https://merojob.com/associate-broadcast-engineer-4/"
    },
    {
      "designation": "Frontend Developer",
      "company": "Ensue Nepal",
      "location": "Gwarko Lalitpur",
      "company_page": "https://merojob.com/front-end-developer-71/"
    },
    {
      "designation": "Laboratory Incharge",
      "company": "A Reputed +2 College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/laboratory-incharge/"
    },
    {
      "designation": "Teachers - Management Department",
      "company": "A Reputed +2 College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/teachers-management-department/"
    },
    {
      "designation": "Teachers - Science Department",
      "company": "A Reputed +2 College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/teachers-science-department/"
    },
    {
      "designation": "Typist (English/Nepali/Math)",
      "company": "A Reputed +2 College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/typist-englishnepalimath/"
    },
    {
      "designation": "Front Office",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/front-office-7/"
    },
    {
      "designation": "Information Center",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/information-center/"
    },
    {
      "designation": "Assistant/Deputy Manager-Market…",
      "company": "Shikhar Insurance Co. Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/assistantdeputy-manager-marketing-department-few/"
    },
    {
      "designation": "Officer-Finance Department (Few)",
      "company": "Shikhar Insurance Co. Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/officer-finance-department-few/"
    },
    {
      "designation": "Executive Secretary",
      "company": "Shikhar Insurance Co. Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/executive-secretary-103/"
    },
    {
      "designation": "Optometrist",
      "company": "Tilganga Institute of Ophthalmology (TIO)",
      "location": "NEP-TIO and Its Branches",
      "company_page": "https://merojob.com/optometrist-10/"
    },
    {
      "designation": "Assistant-Pharmacy",
      "company": "Tilganga Institute of Ophthalmology (TIO)",
      "location": "NEP-TIO and Its Branches",
      "company_page": "https://merojob.com/assistant-pharmacy/"
    },
    {
      "designation": "Surveillance Medical Coordinator",
      "company": "Save The Children",
      "location": "Dhangadi, Mid and Far Western Field Office",
      "company_page": "https://merojob.com/surveillance-medical-coordinator/"
    },
    {
      "designation": "Residential Male Teacher for Bo…",
      "company": "LRI School",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/residential-male-teacher-for-boys-hostel-2/"
    },
    {
      "designation": "Teacher for English Language",
      "company": "LRI School",
      "location": "Kathmandu, Central Development Region, Nepal",
      "company_page": "https://merojob.com/teacher-for-english-language/"
    },
    {
      "designation": "Grade Teachers for Primary Level",
      "company": "LRI School",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/grade-teachers-for-primary-level-4/"
    },
    {
      "designation": "Business Development Implemente…",
      "company": "MiDas Technologies",
      "location": "Kathmandu",
      "company_page":
          "https://merojob.com/1-business-development-implementers-executors/"
    },
    {
      "designation": "Technical Support Officers",
      "company": "MiDas Technologies",
      "location": "Kathmandu",
      "company_page": "https://merojob.com/2-technical-support-officers/"
    },
    {
      "designation": "Accountant",
      "company": "Luck Itta Udhyog",
      "location": "Birgunj",
      "company_page": "https://merojob.com/accountant-1484/"
    },
    {
      "designation": "Communication Officer (Level 6t…",
      "company":
          "Nepal Federation of Savings and Credit Cooperative Unions Limited (NEFSCUN)",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/communication-officer-level-6th/"
    },
    {
      "designation": "Loan Processing Specialist (LPS)",
      "company": "HLE Nepal",
      "location": "Jawalakhel, Patan, Central Development Region, Nepal",
      "company_page": "https://merojob.com/loan-processing-specialist-lps/"
    },
    {
      "designation": "Administration Officer",
      "company": "Genese Software Solutions",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/administration-officer-47/"
    },
    {
      "designation": "Front Office Manager",
      "company": "Waterfront Resort",
      "location": "Pokhara",
      "company_page": "https://merojob.com/front-office-executive-38/"
    },
    {
      "designation": "Site Engineer (Civil / Mechanic…",
      "company": "Dordi Khola Jal Bidyut Company Ltd.",
      "location": "Lamjung, Nepal",
      "company_page":
          "https://merojob.com/site-engineer-civil-mechanical-electrical-3/"
    },
    {
      "designation": "Office Assistant",
      "company": "Lama Estate",
      "location": "Boudha-6, Kathmandu",
      "company_page": "https://merojob.com/office-assistant-351/"
    },
    {
      "designation": "Counselor (India)",
      "company": "NIBC Educational Foundation",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/counselor-india-7/"
    },
    {
      "designation": "Marketing Executive",
      "company": "NIBC Educational Foundation",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/marketing-executive-814/"
    },
    {
      "designation": "Relationship Assistant",
      "company": "An Insurance Company",
      "location": "Kathmandu / Lalitpur",
      "company_page": "https://merojob.com/relationship-assistant-3/"
    },
    {
      "designation": "Java Developer",
      "company": "Cloudyfox Technology",
      "location": "Anamnagar",
      "company_page": "https://merojob.com/java-developer-90/"
    },
    {
      "designation": "Sr. Accountant",
      "company": "Global Service Travel",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/sr-accountant-43/"
    },
    {
      "designation": "Sr. Administrative Officer",
      "company": "Alfa Beta Institute",
      "location": "Buddhanagar, New Baneshwor",
      "company_page": "https://merojob.com/sr-administrative-officer-3/"
    },
    {
      "designation": "Middle School Teachers",
      "company": "Newspaper-Education",
      "location": "Pulchowk, Lalitpur",
      "company_page": "https://merojob.com/middle-school-teachers-7/"
    },
    {
      "designation": "Sales Personnel",
      "company": "Glambisque Pvt. Ltd",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/sales-personnel-8/"
    },
    {
      "designation": "Relationship Manager (Junior Of…",
      "company": "NIC ASIA BANK Ltd.",
      "location": "Inside/Outside Valley",
      "company_page": "https://merojob.com/relationship-manager-18/"
    },
    {
      "designation": "Officer DNA and Talent Manageme…",
      "company": "NIC ASIA BANK Ltd.",
      "location": "Inside Valley",
      "company_page":
          "https://merojob.com/officer-dna-and-talent-management-human-resource-junior-officerofficer/"
    },
    {
      "designation": "National Consultant-Monitoring …",
      "company": "The United Nations Development Programme (UNDP)",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/national-consultant-monitoring-and-evaluation-expert/"
    },
    {
      "designation": "Officer Internal Audit (Junior …",
      "company": "NIC ASIA BANK Ltd.",
      "location": "Inside Valley",
      "company_page":
          "https://merojob.com/officer-internal-audit-junior-officerofficersenior-officer/"
    },
    {
      "designation": "Junior Sales Manager",
      "company": "Ashok Steel Industries Pvt. Ltd.",
      "location":
          "Kathamandu, Dhangadhi, Nepalgunj, Janakpur, Pokhara, Surkhe…",
      "company_page": "https://merojob.com/junior-sales-manager/"
    },
    {
      "designation": "Plant Manager",
      "company": "Hukam Pharmaceuticals Private Limited",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/plant-manager-10/"
    },
    {
      "designation": "Sales Manager / Assistant Sales…",
      "company": "Hotel Yak & Yeti",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/sales-manager-assistant-sales-manager/"
    },
    {
      "designation": "Lecturers",
      "company": "REED Model Secondary School",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/lecturers-20/"
    },
    {
      "designation": "Lecturers",
      "company": "REED Model Secondary School",
      "location": "Swoyambhu, Kathmandu",
      "company_page": "https://merojob.com/lecturers-19/"
    },
    {
      "designation": "Medical Sales Officer",
      "company": "National Healthcare",
      "location": "Kamalpokhari, Kathmandu",
      "company_page": "https://merojob.com/medical-sales-officer-31/"
    },
    {
      "designation": "HR Trainee",
      "company": "United Cement",
      "location": "Naubise",
      "company_page": "https://merojob.com/hr-trainee-4/"
    },
    {
      "designation": "Administrative Manager",
      "company": "United Cement",
      "location": "Naubise",
      "company_page": "https://merojob.com/administrative-manager-18/"
    },
    {
      "designation": "Assistant Officer (Packing Depa…",
      "company": "United Cement",
      "location": "Naubise",
      "company_page": "https://merojob.com/assistant-officer-15/"
    },
    {
      "designation": "Assistant Officer (Logistics De…",
      "company": "United Cement",
      "location": "Naubise",
      "company_page":
          "https://merojob.com/assistant-officer-logistics-department/"
    },
    {
      "designation": "Sales Officer",
      "company": "United Cement",
      "location": "Chitwan, Dhading, Pokhara, Kathmandu",
      "company_page": "https://merojob.com/sales-officer-424/"
    },
    {
      "designation": "Civil Enginner",
      "company": "United Cement",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/civil-enginner/"
    },
    {
      "designation": "काउन्सिलर एण्ड माेटिभेटर (महिला)",
      "company": "मार्गदर्शन बचत तथा ऋण सहकारी संस्था लि.",
      "location": "Pepsicola-32, Kathmandu",
      "company_page": "https://merojob.com/-2102/"
    },
    {
      "designation": "Area Manager",
      "company": "Samar Pharma Company Pvt. Ltd.",
      "location": "Kathmandu/Biratnagar/Birgunj/Narayanghat/Nepalgunj",
      "company_page": "https://merojob.com/area-manager-10/"
    },
    {
      "designation": "व्यवस्थापक (साताैं तह)",
      "company": "बाबियाखर्क बचत तथा ऋण सहकारी संस्था लिमिटेड",
      "location": "शान्तिनगर, काठमाण्डाै",
      "company_page": "https://merojob.com/-2103/"
    },
    {
      "designation": "मार्केटिङ्ग अफिसर",
      "company": "राप्ती सेभिङ्ग एण्ड क्रेडिट काे-अपरेटिभ लि.",
      "location": "Kumaripati, Lalitpur",
      "company_page": "https://merojob.com/-2106/"
    },
    {
      "designation": "मार्केटिङ्ग सुपरभाइजर",
      "company": "राप्ती सेभिङ्ग एण्ड क्रेडिट काे-अपरेटिभ लि.",
      "location": "Kumaripati, Lalitpur",
      "company_page": "https://merojob.com/-2107/"
    },
    {
      "designation": "एकाउन्टेन्ट",
      "company": "राप्ती सेभिङ्ग एण्ड क्रेडिट काे-अपरेटिभ लि.",
      "location": "Kumaripati, Lalitpur",
      "company_page": "https://merojob.com/-2108/"
    },
    {
      "designation": "Technical Engineer",
      "company": "World - Renowned International Engineering Company",
      "location": "Pokhara, Nepal",
      "company_page": "https://merojob.com/technical-engineer-2/"
    },
    {
      "designation": "Assistant Sales Director",
      "company": "Hotel Yak & Yeti",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/assistant-sales-director/"
    },
    {
      "designation": "Accountant",
      "company": "A Reputed Trading Organization",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/accountant-1486/"
    },
    {
      "designation": "Front Desk Officer",
      "company": "A Reputed Trading Organization",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/front-desk-officer-598/"
    },
    {
      "designation": "Associate Sales Officer",
      "company": "Dish Media Network Ltd.",
      "location": "Outside Kathmandu Valley",
      "company_page": "https://merojob.com/associate-sales-officer-12/"
    },
    {
      "designation": "QA Engineer - Data Analytics",
      "company": "Dish Media Network Ltd.",
      "location": "Bhaisepati, Lalitpur",
      "company_page": "https://merojob.com/qa-engineer-data-analytics/"
    },
    {
      "designation": "Senior Analyst (QC)",
      "company": "Amtech Med (P.) Ltd.",
      "location": "Biratnagar, Nepal",
      "company_page": "https://merojob.com/senior-analyst-qc/"
    },
    {
      "designation": "Counsellor",
      "company": "SOS Children's Villages",
      "location": "Bharatpur-8, Garurigunj, Chitwan",
      "company_page": "https://merojob.com/counselor-295/"
    },
    {
      "designation": "Grade Teachers",
      "company": "SOS Children's Villages",
      "location": "Bharatpur-8, Garurigunj, Chitwan",
      "company_page": "https://merojob.com/grade-teachers-55/"
    },
    {
      "designation": "Hostel Warden (Female)",
      "company": "Guheshwari Boarding High School",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/hostel-warden-female-9/"
    },
    {
      "designation": "Lower Secondary English Teacher",
      "company": "Guheshwari Boarding High School",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/lower-secondary-english-teacher-31/"
    },
    {
      "designation": "Senior Maths Teacher",
      "company": "Guheshwari Boarding High School",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/senior-maths-teacher-2/"
    },
    {
      "designation": "Flutter Developer",
      "company": "Delta Tech",
      "location": "Biratnagar",
      "company_page": "https://merojob.com/android-app-developer-13/"
    },
    {
      "designation": "Sales Executive",
      "company": "Laser Travel and Tours P. LTD",
      "location": "Bhaisepati, Lalitpur, Central Development Region, Nepal",
      "company_page": "https://merojob.com/sales-executiv/"
    },
    {
      "designation": "Assistant Manager - (Education …",
      "company": "Galaxy Education and Migration Services(GEMS)",
      "location":
          "Mantra In Out Square, Level 2, New Baneshwor, Kathmandu, Ne…",
      "company_page":
          "https://merojob.com/assistant-manager-education-counsellor-2/"
    },
    {
      "designation": "Web Designer and Developer",
      "company": "Pacific SoftTech",
      "location": "Ranibari 26, Kathmandu",
      "company_page": "https://merojob.com/web-designer-and-developer-6/"
    },
    {
      "designation": "Content Writer",
      "company": "SWS Ltd",
      "location": "Nayabato, Lalitpur",
      "company_page": "https://merojob.com/content-writer-225/"
    },
    {
      "designation": "Full Stack Developer",
      "company": "G. Info services Pvt. ltd",
      "location": "",
      "company_page": "https://merojob.com/full-stack-developer-20/"
    },
    {
      "designation": "Operators",
      "company": "Hukam Pharmaceuticals Private Limited",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/operators-2/"
    },
    {
      "designation": "Medical Representative (MR)",
      "company": "Hukam Pharmaceuticals Private Limited",
      "location": "Dhangadhi, Kathmandu, Dang, Birtamod",
      "company_page": "https://merojob.com/medical-representative-mr-3/"
    },
    {
      "designation": "Production/QC/QA Officers",
      "company": "Hukam Pharmaceuticals Private Limited",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/productionqcqa-officers/"
    },
    {
      "designation": "Chief Finance Officer CFO",
      "company": "Hukam Pharmaceuticals Private Limited",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/chief-finance-officer-cfo-3/"
    },
    {
      "designation": "QC Manager",
      "company": "Hukam Pharmaceuticals Private Limited",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/qc-manager-6/"
    },
    {
      "designation": "Area Sales Manager",
      "company": "Ashok Steel Industries Pvt. Ltd.",
      "location":
          "Kathamandu, Dhangadhi, Nepalgunj, Janakpur, Pokhara, Surkhe…",
      "company_page": "https://merojob.com/area-sales-manager-159/"
    },
    {
      "designation": "Bottling Line Machine Operator",
      "company": "Jawalakhel Group of Industries",
      "location": "Nawalparasi, Nepal",
      "company_page": "https://merojob.com/bottling-line-machine-operator/"
    },
    {
      "designation": "Assistant Manager-Production",
      "company": "Jawalakhel Group of Industries",
      "location":
          "Nawalparasi, Nepal                                                                                ,                                                                            Kathmandu, Nepal",
      "company_page": "https://merojob.com/manager-production/"
    },
    {
      "designation": "Junior Sales Executive",
      "company": "Jawalakhel Group of Industries",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/junior-sales-executive-4/"
    },
    {
      "designation": "Electrician",
      "company": "Jawalakhel Group of Industries",
      "location": "Nawalparasi, Nepal",
      "company_page": "https://merojob.com/electrician-51/"
    },
    {
      "designation": "Procurement Officer",
      "company": "Jawalakhel Group of Industries",
      "location": "Nawalparasi, Nepal",
      "company_page": "https://merojob.com/procurement-officer-87/"
    },
    {
      "designation": "Contract Engineer (Kathmandu/Si…",
      "company": "Peoples Hydropower Company Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/contract-engineer-kathmandusite/"
    },
    {
      "designation": "Sales Account Officer",
      "company": "Shivam Cements",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/sales-account-officer-4/"
    },
    {
      "designation": "Account Officer",
      "company": "Shivam Cements",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/account-officer-317/"
    },
    {
      "designation": "iOS Developer",
      "company": "EBPearls",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/ios-developer-80/"
    },
    {
      "designation": "Geologist",
      "company": "Dordi Khola Jal Bidyut Company Ltd.",
      "location": "Lamjung, Nepal",
      "company_page": "https://merojob.com/geologist-39/"
    },
    {
      "designation": "Graphic Designer",
      "company": "Kirana Nepal",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/graphic-designer-631/"
    },
    {
      "designation": "Amazon Product Research Analyst",
      "company": "ZEN IT HUB",
      "location": "Baluwatar, Kathmandu",
      "company_page": "https://merojob.com/amazon-product-research-analyst/"
    },
    {
      "designation": "People & Culture Manager",
      "company": "HLE Nepal",
      "location": "Jawalakhel, Lalitpur, Nepal",
      "company_page": "https://merojob.com/people-culture-manager/"
    },
    {
      "designation": "Hospital Manager",
      "company": "Nepal Netra Jyoti Sangh",
      "location": "Bhairahawa",
      "company_page": "https://merojob.com/hospital-manager-12/"
    },
    {
      "designation": "Account Assistant",
      "company": "Nepal Netra Jyoti Sangh",
      "location": "Tripureshwor, Kathmandu",
      "company_page": "https://merojob.com/account-assistant-147/"
    },
    {
      "designation": "Accountant",
      "company": "Saral Urja Nepal",
      "location": "Gyaneswar, Kathmandu",
      "company_page": "https://merojob.com/accountant-1482/"
    },
    {
      "designation": "F&B Senior Supervisor",
      "company": "Waterfront Resort",
      "location": "Pokhara",
      "company_page": "https://merojob.com/fb-senior-supervisor/"
    },
    {
      "designation": "Housekeeping Senior Supervisor",
      "company": "Waterfront Resort",
      "location": "Pokhara",
      "company_page": "https://merojob.com/housekeeping-senior-supervisor/"
    },
    {
      "designation": "Librarian (Female)",
      "company": "A Reputed College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/librarian-101/"
    },
    {
      "designation": "School Wing Teachers",
      "company": "NASA International Education Network",
      "location": "Gairigaun, Tinkune, Kathmandu",
      "company_page": "https://merojob.com/school-wing-teachers/"
    },
    {
      "designation": "+2 Wing Teachers",
      "company": "NASA International Education Network",
      "location": "Gairigaun, Tinkune, Kathmandu",
      "company_page": "https://merojob.com/2-wing-teachers/"
    },
    {
      "designation": "Bachelor Wing Teachers",
      "company": "NASA International Education Network",
      "location": "Gairigaun, Tinkune, Kathmandu",
      "company_page": "https://merojob.com/bachelor-wing-teachers/"
    },
    {
      "designation": "Administration",
      "company": "NASA International Education Network",
      "location": "Gairigaun, Tinkune, Kathmandu",
      "company_page": "https://merojob.com/administration-3/"
    },
    {
      "designation": "Montessori Asst. In-Charge",
      "company": "The New Summit Secondary School",
      "location": "Maitidevi Chowk, Kathmandu",
      "company_page": "https://merojob.com/montessori-asst-in-charge/"
    },
    {
      "designation": "Account Officer",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/account-officer-321/"
    },
    {
      "designation": "Accountant Assistant",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/accountant-assistant-9/"
    },
    {
      "designation": "Lab Assistant",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/lab-assistant-60/"
    },
    {
      "designation": "Sports Teacher",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/sports-teacher-25/"
    },
    {
      "designation": "Regional Marketing Head",
      "company": "Kantipur Publications",
      "location": "Pokhara, Nepal",
      "company_page": "https://merojob.com/regional-marketing-head/"
    },
    {
      "designation": "Admin Officer",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/admin-officer-112/"
    },
    {
      "designation": "IELTS Instructor",
      "company": "Subhagriha Institute and Consultancy",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/ielts-instructor-76/"
    },
    {
      "designation": "Senior Php Developer (Biratnaga…",
      "company": "Delta Tech",
      "location": "Biratnagar",
      "company_page": "https://merojob.com/senior-php-developer-biratnagar/"
    },
    {
      "designation": "Senior / Procurement Officer",
      "company": "OM Hospital & Research Center Pvt. Ltd.",
      "location": "Kathmandu",
      "company_page": "https://merojob.com/seniorprocurement-officer/"
    },
    {
      "designation": "Office Secretary",
      "company": "SOS Manpower Service",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/business-development-officer-349/"
    },
    {
      "designation": "Digital Marketing Specialist",
      "company": "The British College",
      "location": "Thapathali, Kathmandu",
      "company_page": "https://merojob.com/digital-marketing-specialist-13/"
    },
    {
      "designation": "Medical Sales Officer (MSO)",
      "company": "Arya Pharma",
      "location": "Pokhara and Narayanghat",
      "company_page": "https://merojob.com/medical-sales-officer-mso-11/"
    },
    {
      "designation": "Program Director",
      "company": "Global College",
      "location": "Baneshwor, Kathmandu",
      "company_page": "https://merojob.com/program-director-10/"
    },
    {
      "designation": "Executive Vice-Principal",
      "company": "Global College",
      "location":
          "Baneshwor, Kathmandu                                                                                ,                                                                            Kathmandu, Nepal",
      "company_page": "https://merojob.com/executive-vice-principal/"
    },
    {
      "designation": "Assistant Manager (Managerial L…",
      "company": "Mega Bank Limited",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/assistant-manager-managerial-level/"
    },
    {
      "designation": "Assistant / Senior Assistant / …",
      "company": "Mega Bank Limited",
      "location":
          "Kathmandu, Jomsom, Rautamai Gaunpalika (Udayapur), Kumyaak …",
      "company_page":
          "https://merojob.com/assistant-senior-assistant-supervisor-2/"
    },
    {
      "designation": "Junior Officer / Officer / Seni…",
      "company": "Mega Bank Limited",
      "location":
          "Kathmandu, Pokhara, Itahari, Narayangarh, Dhading, Tulsipur…",
      "company_page":
          "https://merojob.com/junior-officer-officer-senior-officer-officer-level/"
    },
    {
      "designation": "Programme Associate (Technology…",
      "company": "World Food Programme",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/programme-associate-technology-and-innovations/"
    },
    {
      "designation": "Operation Manager",
      "company": "Stuti Mero Mart",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/operation-manager-92/"
    },
    {
      "designation": "Outlet Incharge",
      "company": "Stuti Mero Mart",
      "location": "Kathmandu",
      "company_page": "https://merojob.com/outlet-incharge-4/"
    },
    {
      "designation": "Purchase Manager",
      "company": "Stuti Mero Mart",
      "location": "Kathmandu",
      "company_page": "https://merojob.com/purchase-manager-19/"
    },
    {
      "designation": "Lecturer",
      "company": "A Reputed College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/lecturer-98/"
    },
    {
      "designation": "Receptionist",
      "company": "A Reputed School",
      "location": "Banasthali",
      "company_page": "https://merojob.com/receptionist-1153/"
    },
    {
      "designation": "Assistant - Accounts",
      "company": "Janaki Technology",
      "location": "Bakundole, Lalitpur",
      "company_page": "https://merojob.com/assistant-accounts-5/"
    },
    {
      "designation": "Lower Secondary Level Teachers",
      "company": "Fluorescent Secondary School (FSS)",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/lower-secondary-level-teachers-28/"
    },
    {
      "designation": "नायव प्रमुख कार्यकारी अधिकृत",
      "company": "इन्फिनिटी लघुवित्त वित्तीय संस्था लि.",
      "location": "गैंडाकाेट नगरपालिका वडा नं. २, कालिगण्डकीचाेक, नवलपुर",
      "company_page": "https://merojob.com/-2089/"
    },
    {
      "designation": "Lower  Division Clerk",
      "company": "Military Pension Branch",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/lower-division-clerk/"
    },
    {
      "designation": "कनिष्ठ अधिकृत / अधिकृत / वरिष्ठ अ…",
      "company": "इन्फिनिटी लघुवित्त वित्तीय संस्था लि.",
      "location": "गैंडाकाेट नगरपालिका वडा नं. २, कालिगण्डकीचाेक, नवलपुर",
      "company_page": "https://merojob.com/-2090/"
    },
    {
      "designation": "व्यवस्थापन प्रशिक्षार्थी - केही",
      "company": "इन्फिनिटी लघुवित्त वित्तीय संस्था लि.",
      "location": "गैंडाकाेट नगरपालिका वडा नं. २, कालिगण्डकीचाेक, नवलपुर",
      "company_page": "https://merojob.com/-2091/"
    },
    {
      "designation": "वरिष्ठ सहायक (शाखा प्रमुख) - केही",
      "company": "इन्फिनिटी लघुवित्त वित्तीय संस्था लि.",
      "location": "गैंडाकाेट नगरपालिका वडा नं. २, कालिगण्डकीचाेक, नवलपुर",
      "company_page": "https://merojob.com/-2092/"
    },
    {
      "designation": "Education & Visa Counselor to A…",
      "company": "4Nations International (Nepal)",
      "location": "Kathmandu",
      "company_page":
          "https://merojob.com/education-visa-counselor-to-australia/"
    },
    {
      "designation": "Executive Assistant and Office …",
      "company": "4Nations International (Nepal)",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/executive-assistant-and-office-manager/"
    },
    {
      "designation": "Receptionist",
      "company": "4Nations International (Nepal)",
      "location": "Kathmandu",
      "company_page": "https://merojob.com/receptionist-1152/"
    },
    {
      "designation": "UI/UX Designer",
      "company": "Diagonal Technologies",
      "location": "Pulchowk Gabahal Road, Patan, Nepal",
      "company_page":
          "https://merojob.com/uiux-designer-work-from-home-up-to-8xmonth/"
    },
    {
      "designation": "Marketing Officer",
      "company": "InfoDevelopers",
      "location": "Sanepa, Lalitpur",
      "company_page": "https://merojob.com/dynamic-marketing-officer/"
    },
    {
      "designation": "ASP.NET Developers",
      "company": "InfoDevelopers",
      "location": "Sanepa, Patan",
      "company_page": "https://merojob.com/aspnet-developers-12/"
    },
    {
      "designation": "Residential Manager",
      "company": "Nana Holiday Homes",
      "location": "Amaltari, Nawalparasi",
      "company_page": "https://merojob.com/residential-manager-4/"
    },
    {
      "designation": "Marketing Officer",
      "company": "Brevin Creation",
      "location": "New Baneshwor",
      "company_page": "https://merojob.com/marketing-officer-868/"
    },
    {
      "designation": "Wordpress Developer",
      "company": "Brevin Creation",
      "location": "Mid Baneshwor",
      "company_page": "https://merojob.com/wordpress-developer-154/"
    },
    {
      "designation": "Magento Developer",
      "company": "EBPearls",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/magento-developer-25/"
    },
    {
      "designation": "Account Manager",
      "company": "Miyon International",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/account-manager-59/"
    },
    {
      "designation": "Primary Teachers",
      "company": "Bodhisattva Vihara (BV)",
      "location": "Budhanilkantha (1.5 km North from the Narayanthan Temple)",
      "company_page": "https://merojob.com/primary-teachers-24/"
    },
    {
      "designation": "Science Teacher (Secondary Leve…",
      "company": "Secondary Level School",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/science-teacher-secondary-level-15/"
    },
    {
      "designation": "Manager (Security/Administratio…",
      "company": "Korea International Cooperation Agency (KOICA)",
      "location": "Jwagal, Lalitpur",
      "company_page": "https://merojob.com/manager-securityadministration/"
    },
    {
      "designation": "Sales & Marketing Executive",
      "company": "ARYAL INTERNATIONAL HOTEL",
      "location": "New Baneshwor, Kathmandu",
      "company_page": "https://merojob.com/sales-marketing-executive-97/"
    },
    {
      "designation": "Finance Head",
      "company": "ARYAL INTERNATIONAL HOTEL",
      "location": "New Baneshwor, Kathmandu",
      "company_page": "https://merojob.com/finance-head-11/"
    },
    {
      "designation": "Front Desk Personnel",
      "company": "ARYAL INTERNATIONAL HOTEL",
      "location": "New Baneshwor, Kathmandu",
      "company_page": "https://merojob.com/front-desk-personnel-3/"
    },
    {
      "designation": "Tender Engineer",
      "company": "World - Renowned International Engineering Company",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/tender-engineer-2/"
    },
    {
      "designation": "National Human Rights Advisor",
      "company": "The United Nations Resident Coordinator Office (UNRCO)",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/national-human-rights-advisor/"
    },
    {
      "designation": "Service Manager / Operation In-…",
      "company": "Kamana Sewa Bikas Bank Ltd.",
      "location": "",
      "company_page":
          "https://merojob.com/service-manager-operation-in-charge-senior-assistant-to-supervisor/"
    },
    {
      "designation": "Relationship Manager - Senior A…",
      "company": "Kamana Sewa Bikas Bank Ltd.",
      "location":
          "Besisahar, Kohalpur, Mainabagar, Mahendranagar, Itahari, Ba…",
      "company_page":
          "https://merojob.com/relationship-manager-senior-assistant-to-junior-officer/"
    },
    {
      "designation": "Branch Manager - Junior Officer…",
      "company": "Kamana Sewa Bikas Bank Ltd.",
      "location":
          "Birgunj, Dhadng, Dumkibas, Khaireni, Khairenitar, Jaspur, R…",
      "company_page":
          "https://merojob.com/branch-manager-junior-officer-to-assistant-manager/"
    },
    {
      "designation": "English Department Head",
      "company": "A Reputed School",
      "location": "New Baneshwor, Kathmandu",
      "company_page": "https://merojob.com/english-department-head-4/"
    },
    {
      "designation": "Deputy Chief of Party (DCOP)/Ag…",
      "company":
          "Knowledge-based Integrated Sustainable Agriculture in Nepal (KISAN) II Pro…",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/deputy-chief-of-party-dcopagriculture-d3/"
    },
    {
      "designation": "Officer Business Process Re-eng…",
      "company": "NIC ASIA BANK Ltd.",
      "location": "Inside Valley",
      "company_page":
          "https://merojob.com/officer-business-process-re-engineering-junior-officerofficer/"
    },
    {
      "designation": "Experience Enrichment Departmen…",
      "company": "NIC ASIA BANK Ltd.",
      "location": "Inside Valley",
      "company_page":
          "https://merojob.com/experience-enrichment-department-customer-care-manager-junior-officerofficer/"
    },
    {
      "designation": "Experience Enrichment Departmen…",
      "company": "NIC ASIA BANK Ltd.",
      "location": "Inside Valley",
      "company_page":
          "https://merojob.com/experience-enrichment-department-customer-care-supervisor-senior-assistantsupervisor/"
    },
    {
      "designation": "बजार प्रतिनिधिहरू",
      "company": "मिनी बचत तथा ऋण सहकारी संस्था लि.",
      "location": "काठमाडौं",
      "company_page": "https://merojob.com/-2093/"
    },
    {
      "designation": "मार्केटिङ अफिसर",
      "company": "मिनी बचत तथा ऋण सहकारी संस्था लि.",
      "location": "काठमाडौं",
      "company_page": "https://merojob.com/-2094/"
    },
    {
      "designation": "कर्जा अधिकृत",
      "company": "मिनी बचत तथा ऋण सहकारी संस्था लि.",
      "location": "काठमाडौं",
      "company_page": "https://merojob.com/-2095/"
    },
    {
      "designation": "Community Liaison Specialist",
      "company": "CDM Smith",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/community-liaison-specialist/"
    },
    {
      "designation": "Program Coordinator (+2)",
      "company": "An Education Institution",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/program-coordinator-2-2/"
    },
    {
      "designation": "Pharmacist (Female)",
      "company": "3S Pharmacy P.Ltd",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/pharmacist-male-female/"
    },
    {
      "designation": "Sales Assistant (Female)",
      "company": "3S Pharmacy P.Ltd",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/sales-assistant-female-5/"
    },
    {
      "designation": "Architect",
      "company": "21 Alphas Design Team",
      "location": "Pokhara",
      "company_page": "https://merojob.com/architect-271/"
    },
    {
      "designation": "Civil Engineer",
      "company": "SafBon Water Service (Holding) Inc.",
      "location": "Sankhamul, Kathmandu",
      "company_page": "https://merojob.com/civil-engineer-few-10/"
    },
    {
      "designation": "Junior Trainee Assistant - Few",
      "company": "Kamana Sewa Bikas Bank Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/junior-trainee-assistant-few-2/"
    },
    {
      "designation": "Provincial Managers",
      "company": "Citizens Bank International Ltd.",
      "location": "Birgunj, Surkhet",
      "company_page": "https://merojob.com/provincial-managers/"
    },
    {
      "designation": "Branch Managers-Few",
      "company": "Citizens Bank International Ltd.",
      "location":
          "Biratnagar, Dharan, Itahari, Damak, Nepalgunj, Birgunj, Pok…",
      "company_page": "https://merojob.com/branch-managers-few-4/"
    },
    {
      "designation": "Deposit Marketing Officer-Few",
      "company": "Citizens Bank International Ltd.",
      "location": "Inside & Outside Valley  Branches",
      "company_page": "https://merojob.com/deposit-marketing-officer-few/"
    },
    {
      "designation": "Relationship Officer-Few",
      "company": "Citizens Bank International Ltd.",
      "location": "Inside & Outside Valley  Branches",
      "company_page": "https://merojob.com/relationship-officer-few-3/"
    },
    {
      "designation": "Primary Teachers",
      "company": "Newspaper-Education",
      "location": "Pulchowk, Lalitpur",
      "company_page": "https://merojob.com/primary-teachers-25/"
    },
    {
      "designation": "Hospital Operation Manager",
      "company": "Nepal Cancer Hospital",
      "location": "Harisiddhi, Lalitpur, Nepal",
      "company_page": "https://merojob.com/hospital-operation-manager/"
    },
    {
      "designation": "Radiotherapy Technician",
      "company": "Nepal Cancer Hospital",
      "location": "Harisiddhi, Lalitpur, Nepal",
      "company_page": "https://merojob.com/radiotherapy-technician/"
    },
    {
      "designation": "Assistant Front Office Manager",
      "company": "Hotel Yak & Yeti",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/assistant-front-office-manager-7/"
    },
    {
      "designation": "Marketing Officer",
      "company": "Nepal Cancer Hospital",
      "location": "Harisiddhi, Lalitpur, Nepal",
      "company_page": "https://merojob.com/marketing-officer-869/"
    },
    {
      "designation": "Head Cook",
      "company": "A Reputed College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/head-cook-15/"
    },
    {
      "designation": "Psychological Counselor (Female)",
      "company": "A Reputed College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/psychological-counselor-female-2/"
    },
    {
      "designation": "Sales Representative",
      "company": "VIANET",
      "location": "Inside and Outside Valley",
      "company_page": "https://merojob.com/sales-representative-142/"
    },
    {
      "designation": "Assistant-Anesthesia (Few)",
      "company": "Tilganga Institute of Ophthalmology (TIO)",
      "location": "NEP-TIO",
      "company_page": "https://merojob.com/assistant-anesthesia-few/"
    },
    {
      "designation": "Trainee Operators",
      "company": "Jawalakhel Group of Industries",
      "location": "Birgunj, Nawalparasi, Chitwan",
      "company_page": "https://merojob.com/trainee-operataors/"
    },
    {
      "designation": "Senior Admin Executive",
      "company": "Nepal Republic Media",
      "location": "Bagdurbar, Kathmandu",
      "company_page": "https://merojob.com/senior-admin-executive-2/"
    },
    {
      "designation": "Hostel Warden (Female)",
      "company": "A Reputed School",
      "location": "New Baneshwor, Kathmandu",
      "company_page": "https://merojob.com/hostel-warden-female-8/"
    },
    {
      "designation": "English Teacher (Secondary Leve…",
      "company": "A Reputed School",
      "location": "New Baneshwor, Kathmandu",
      "company_page": "https://merojob.com/english-teacher-secondary-level-12/"
    },
    {
      "designation": "अाइटी अधिकृत (छैंटाैं तह)",
      "company": "बाबियाखर्क बचत तथा ऋण सहकारी संस्था लिमिटेड",
      "location": "शान्तिनगर, काठमाण्डाै",
      "company_page": "https://merojob.com/-2104/"
    },
    {
      "designation": "बजार अधिकृत (पुरूष)",
      "company": "मार्गदर्शन बचत तथा ऋण सहकारी संस्था लि.",
      "location": "Pepsicola-32, Kathmandu",
      "company_page": "https://merojob.com/-2097/"
    },
    {
      "designation": "ऋण अधिकृत (पुरूष)",
      "company": "मार्गदर्शन बचत तथा ऋण सहकारी संस्था लि.",
      "location": "Pepsicola-32, Kathmandu",
      "company_page": "https://merojob.com/-2098/"
    },
    {
      "designation": "रिसेप्सनिष्ट (महिला)",
      "company": "मार्गदर्शन बचत तथा ऋण सहकारी संस्था लि.",
      "location": "Pepsicola-32, Kathmandu",
      "company_page": "https://merojob.com/-2099/"
    },
    {
      "designation": "क्यासियर (महिला)",
      "company": "मार्गदर्शन बचत तथा ऋण सहकारी संस्था लि.",
      "location": "Pepsicola-32, Kathmandu",
      "company_page": "https://merojob.com/-2100/"
    },
    {
      "designation": "बजार प्रर्वदन प्रतिनिधी",
      "company": "मार्गदर्शन बचत तथा ऋण सहकारी संस्था लि.",
      "location": "Pepsicola-32, Kathmandu",
      "company_page": "https://merojob.com/-2101/"
    },
    {
      "designation": "Project Leader/Sales",
      "company": "Build Up Nepal",
      "location": "Kathmandu, Central Development Region, Nepal",
      "company_page": "https://merojob.com/project-leadersales/"
    },
    {
      "designation": "Smart Centre Supervisor",
      "company": "Smart Telecom",
      "location": "Bhaktapur, Kathmandu",
      "company_page": "https://merojob.com/smart-centre-supervisor-3/"
    },
    {
      "designation": "Senior PHP Engineer",
      "company": "UBA Solutions",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/senior-php-engineer-8/"
    },
    {
      "designation": "BI Executive- Sales",
      "company": "Smart Telecom",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/bi-executive-sales/"
    },
    {
      "designation": "Translator",
      "company": "Wordinvent",
      "location": "Mahalaxmi-6, Lalitpur (Opposite of Mahalaxmi municipality)",
      "company_page": "https://merojob.com/translator-13/"
    },
    {
      "designation": "HR Coordinator",
      "company": "Sky Light",
      "location": "",
      "company_page": "https://merojob.com/hr-coordinator-7/"
    },
    {
      "designation": "Secretary (Female)",
      "company": "Target Consultant Human Resource Pvt. Ltd.",
      "location": "Sinamangal, Kathmandu",
      "company_page": "https://merojob.com/secretary-76/"
    },
    {
      "designation": "Accountant (Male/Female)",
      "company": "Target Consultant Human Resource Pvt. Ltd.",
      "location": "Sinamangal, Kathmandu",
      "company_page": "https://merojob.com/accountant-malefemale-3/"
    },
    {
      "designation": "Documentation (Male/Female)",
      "company": "Target Consultant Human Resource Pvt. Ltd.",
      "location": "Sinamangal, Kathmandu",
      "company_page": "https://merojob.com/documentation-malefemale/"
    },
    {
      "designation": "Sr. Communications and Outreach…",
      "company":
          "Knowledge-based Integrated Sustainable Agriculture in Nepal (KISAN) II Pro…",
      "location": "Kathmandu, Nepal",
      "company_page":
          "https://merojob.com/sr-communications-and-outreach-manager-p6d1/"
    },
    {
      "designation": "Secondary Level Teachers",
      "company": "Fluorescent Secondary School (FSS)",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/secondary-level-teachers-51/"
    },
    {
      "designation": "Primary Level Teacher",
      "company": "Fluorescent Secondary School (FSS)",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/primary-level-teacher-12/"
    },
    {
      "designation": "CSR Manager",
      "company": "SK Ventures",
      "location": "Tangal, Gairidhara, Kathmandu",
      "company_page": "https://merojob.com/csr-manager/"
    },
    {
      "designation": "Architech Engineer",
      "company": "SK Ventures",
      "location": "Tangal, Gairidhara, Kathmandu",
      "company_page": "https://merojob.com/artitech-engineer/"
    },
    {
      "designation": "Sub Overseer",
      "company": "SK Ventures",
      "location": "Tangal, Gairidhara, Kathmandu",
      "company_page": "https://merojob.com/sub-overseer-7/"
    },
    {
      "designation": "Finance Officer",
      "company": "SK Ventures",
      "location": "Tangal, Gairidhara, Kathmandu",
      "company_page": "https://merojob.com/finance-officer-174/"
    },
    {
      "designation": "Legal Consultant",
      "company": "SK Ventures",
      "location": "Tangal, Gairidhara, Kathmandu",
      "company_page": "https://merojob.com/legal-consultant-3/"
    },
    {
      "designation": "Client Relations Officer",
      "company": "SK Ventures",
      "location": "Tangal, Gairidhara, Kathmandu",
      "company_page": "https://merojob.com/civil-engineer-371/"
    },
    {
      "designation": "Country Director",
      "company": "Javra Software",
      "location": "Kathmandu",
      "company_page": "https://merojob.com/country-director-20/"
    },
    {
      "designation": "Finance and Admin Officer",
      "company": "Umbrella Organisation Nepal ( UON )",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/finance-and-admin-officer-33/"
    },
    {
      "designation": "Media Coordinator",
      "company": "Colleges Nepal",
      "location":
          "Putalisadak Opposite tirupati Hotel. IT training Nepal buil…",
      "company_page": "https://merojob.com/media-coordinator-7/"
    },
    {
      "designation": "Business Development Executive",
      "company": "Netfiniti Pvt. Ltd.",
      "location": "Gairidhara, Kathmandu, Central Development Region, Nepal",
      "company_page": "https://merojob.com/engineer-solution-consultant/"
    },
    {
      "designation": "Mid Level PHP Developer",
      "company": "Longtail e-Media",
      "location": "Sankhamul, Lalitpur",
      "company_page": "https://merojob.com/mid-level-php-developer-6/"
    },
    {
      "designation": "Web Designer",
      "company": "Socheko.com",
      "location": "Kathmandu",
      "company_page": "https://merojob.com/web-desinger-3/"
    },
    {
      "designation": "Associate Procurement Officer",
      "company": "Dish Media Network",
      "location": "Bhaisepati",
      "company_page": "https://merojob.com/associate-procurement-officer-2/"
    },
    {
      "designation": "Associate Legal Officer",
      "company": "Dish Media Network",
      "location": "Bhaisepati",
      "company_page": "https://merojob.com/associate-legal-officer-2/"
    },
    {
      "designation": "Mid Level QA Engineer",
      "company": "Logicabeans",
      "location": "Hattisar, Kathmandu, Nepal",
      "company_page": "https://merojob.com/mid-level-qa-engineer-4/"
    },
    {
      "designation": "Senior QA Engineer",
      "company": "Logicabeans",
      "location": "Logica Beans, Hattisar",
      "company_page": "https://merojob.com/senior-qa-engineer-8/"
    },
    {
      "designation": "Marketing Officer",
      "company": "SDG Global Imports Pvt. Ltd.",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/marketing-officers-40/"
    },
    {
      "designation": "Programme Director (PD)",
      "company": "Umbrella Organisation Nepal ( UON )",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/programme-director-pd/"
    },
    {
      "designation": "IOS Developer",
      "company": "LocalLoyalties",
      "location": "Chitwan, Nepal",
      "company_page": "https://merojob.com/ios-developer-81/"
    },
    {
      "designation": "Java Developer",
      "company": "Magnus Consulting Group",
      "location": "Milan Chowk, Baneshwor, Kathmandu.",
      "company_page": "https://merojob.com/java-developer-91/"
    },
    {
      "designation": "Executive Secretary",
      "company": "Makawanpur Chamber of Commerce & Industry",
      "location": "Hetauda",
      "company_page": "https://merojob.com/executive-secretary-102/"
    },
    {
      "designation": "Accountant",
      "company": "Palchowk Mai",
      "location": "Naxal, Kathmandu",
      "company_page": "https://merojob.com/accountant-1466/"
    },
    {
      "designation": "Desktop",
      "company": "Pentagon International College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/desktop/"
    },
    {
      "designation": "Assistant Teachers",
      "company": "A Reputed +2 College",
      "location": "Kathmandu, Nepal",
      "company_page": "https://merojob.com/assistant-teachers-10/"
    }
  ];

  Widget indexWidgetBuilder(BuildContext context, index) {
    return Card(
        child: ListTile(
      leading: CircleAvatar(
        backgroundImage: AssetImage('images/image1.jpg'),
      ),
      title: Text(json[index]['designation']),
      subtitle: Text(json[index]['company']),
    ));
  }

  return ListView.builder(
    itemCount: json.length,
    itemBuilder: (context, index) {
      return Card(
          child: ListTile(
        leading: CircleAvatar(
          backgroundImage: AssetImage('images/image1.jpg'),
        ),
        title: Text(json[index]['designation']),
        subtitle: Text(json[index]['company']),
      ));
    },
  );
}

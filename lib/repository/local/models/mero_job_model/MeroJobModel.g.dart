// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'MeroJobModel.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MeroJobModelResponse _$MeroJobModelResponseFromJson(Map<String, dynamic> json) {
  return MeroJobModelResponse()
    ..hasNextPage = json['hasNextPage'] as bool
    ..status = json['status'] as String
    ..message = json['message'] as String
    ..data = (json['data'] as List)
        ?.map((e) =>
            e == null ? null : MeroJobModel.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$MeroJobModelResponseToJson(
        MeroJobModelResponse instance) =>
    <String, dynamic>{
      'hasNextPage': instance.hasNextPage,
      'status': instance.status,
      'message': instance.message,
      'data': instance.data
    };

MeroJobModel _$MeroJobModelFromJson(Map<String, dynamic> json) {
  return MeroJobModel()
    ..designation = json['designation'] as String
    ..company = json['company'] as String
    ..location = json['location'] as String
    ..company_page = json['company_page'] as String
    ..category_type = json['category_type'] as String
    ..designation_type = json['designation_type'] as String
    ..expires = json['expires'] as String
    ..views = json['views'] as String;
}

Map<String, dynamic> _$MeroJobModelToJson(MeroJobModel instance) =>
    <String, dynamic>{
      'designation': instance.designation,
      'company': instance.company,
      'location': instance.location,
      'company_page': instance.company_page,
      'category_type': instance.category_type,
      'designation_type': instance.designation_type,
      'expires': instance.expires,
      'views': instance.views
    };


import 'package:json_annotation/json_annotation.dart';
part 'MeroJobModel.g.dart';

/*
  Response
 */
@JsonSerializable()
class MeroJobModelResponse{

  MeroJobModelResponse(){}

  String _status;
  String _message;
  List<MeroJobModel> _data;
  bool hasNextPage;

  String get status => _status;

  set status(String value) {
    _status = value;
  }

  String get message => _message;

  set message(String value) {
    _message = value;
  }

  List<MeroJobModel> get data => _data;

  set data(List<MeroJobModel> value) {
    _data = value;
  }


  //converters
  factory MeroJobModelResponse.fromJson(Map<String, dynamic> json)=> _$MeroJobModelResponseFromJson(json);



}


/*
  Entity
 */
@JsonSerializable()
class MeroJobModel {

  MeroJobModel(){}

  String _designation;
  String _company;
  String _location;
  String _company_page;
  String _views;
  String _expires;
  String _designation_type;
  String _category_type;

  String get designation => _designation;

  set designation(String value) {
    _designation = value;
  }

  String get company => _company;

  set company(String value) {
    _company = value;
  }

  String get location => _location;

  set location(String value) {
    _location = value;
  }

  String get company_page => _company_page;

  set company_page(String value) {
    _company_page = value;
  }

  String get category_type => _category_type;

  set category_type(String value) {
    _category_type = value;
  }

  String get designation_type => _designation_type;

  set designation_type(String value) {
    _designation_type = value;
  }

  String get expires => _expires;

  set expires(String value) {
    _expires = value;
  }

  String get views => _views;

  set views(String value) {
    _views = value;
  }


  //converters
  factory MeroJobModel.fromJson(Map<String,dynamic> jsonString)=>_$MeroJobModelFromJson(jsonString);
  Map<String,dynamic> toJson(MeroJobModel instance)=>_$MeroJobModelToJson(instance);


}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'NetworkService.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

class _$JobApiService extends JobApiService {
  _$JobApiService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  final definitionType = JobApiService;

  Future<Response> getJobs() {
    final $url = '/getJobs';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }
}

class _$MeroJobApiService extends MeroJobApiService {
  _$MeroJobApiService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  final definitionType = MeroJobApiService;

  Future<Response> getMeroJobPagination() {
    final $url = '/get_merojob_pagination';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }
}

class _$UserApiService extends UserApiService {
  _$UserApiService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  final definitionType = UserApiService;

  Future<Response> getUsers() {
    final $url = '/getJobs';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }
}

class _$FirebaseApiService extends FirebaseApiService {
  _$FirebaseApiService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  final definitionType = FirebaseApiService;

  Future<Response> sendToken() {
    final $url = '/getJobs';
    final $request = Request('GET', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }
}

class _$NetworkService extends NetworkService {
  _$NetworkService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  final definitionType = NetworkService;
}

import 'package:chopper/chopper.dart';
import 'package:jagir/repository/local/models/mero_job_model/MeroJobModel.dart';

part 'NetworkService.chopper.dart';


/*
  Create your services for common components like users,posts,firebase,auth etc
  todo: You cannot create multiple file services, you have to create in a single file

  flutter packages pub run build_runner build

 */


@ChopperApi()
abstract class JobApiService extends ChopperService{
  @Get(path:'/getJobs')
  Future<Response> getJobs();

}


@ChopperApi()
abstract class MeroJobApiService extends ChopperService{

  @Get(path:'/get_merojob_pagination')
  Future<Response> getMeroJobPagination();

}

@ChopperApi()
abstract class UserApiService extends ChopperService{
  @Get(path:'/getJobs')
  Future<Response> getUsers();

}

@ChopperApi()
abstract class FirebaseApiService extends ChopperService{
  @Get(path:'/getJobs')
  Future<Response> sendToken();

}

@ChopperApi()
abstract class NetworkService extends ChopperService{


  static NetworkService create(){
    final client=ChopperClient(
      baseUrl: 'http://192.168.1.109:3000',
      services: [
        _$JobApiService(),
        _$FirebaseApiService(),
        _$UserApiService(),
        _$MeroJobApiService(),
      ],
      converter: JsonConverter()
    );
    return _$NetworkService(client);
  }

}